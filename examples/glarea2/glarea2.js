/*
 * SPDX-FileCopyrightText: 2015 Emmanuele Bassi
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Adapted from glarea-example by Emmanuele Bassi.
 * https://github.com/ebassi/glarea-example
 * https://www.bassi.io/articles/2015/02/17/using-opengl-with-gtk
 */

/*
 * A simple example of how to use OpenGL and the GtkGLArea widget.
 *
 * Launch with the command "gjs -I . glarea2.js"
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk, Updwn: gl } = imports.gi;
const { Buffer, Program, Shader, ShaderList, VertexArray } = imports.glarea2Objects;
const directory = Gio.File.new_for_path('.');

// The constant vertex data (position and color).
const vertexData = [
     0.0,  0.500, 0,    1, 0, 0,
     0.5, -0.366, 0,    0, 1, 0,
    -0.5, -0.366, 0,    0, 0, 1,
];

const Window = GObject.registerClass({
    GTypeName: 'GlareaAppWindow',
    Template: directory.get_child('glarea2-window.ui').get_uri(),
    InternalChildren: ['gl_drawing_area', 'x_adjustment', 'y_adjustment', 'z_adjustment'],
}, class Window extends Gtk.ApplicationWindow {
    _init(params) {
        this.gl_init = this._glInit.bind(this);
        this.gl_fini = this._glFini.bind(this);
        this.gl_draw = this._glDraw.bind(this);
        this.animate_toggled = this._animateToggled.bind(this);
        this.adjustment_changed = this._adjustmentChanged.bind(this);

        super._init(params);

        // The adjustments we use to control the rotation angles.
        this._adjustments = [this._x_adjustment, this._y_adjustment, this._z_adjustment];
        this._rotationAngles = [0, 0, 0];
        this._area = this._gl_drawing_area;

        // The model-view-projection matrix,
        // initialize the matrix as an identity matrix.
        this._mvp = [
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1,
        ];
    }

    _computeMvp() {
        let [phi, theta, psi] = this._rotationAngles;

        let x = phi * (Math.PI / 180);
        let y = theta * (Math.PI / 180);
        let z = psi * (Math.PI / 180);
        let c1 = Math.cos(x), s1 = Math.sin(x);
        let c2 = Math.cos(y), s2 = Math.sin(y);
        let c3 = Math.cos(z), s3 = Math.sin(z);
        let c3c2 = c3 * c2;
        let s3c1 = s3 * c1;
        let c3s2s1 = c3 * s2 * s1;
        let s3s1 = s3 * s1;
        let c3s2c1 = c3 * s2 * c1;
        let s3c2 = s3 * c2;
        let c3c1 = c3 * c1;
        let s3s2s1 = s3 * s2 * s1;
        let c3s1 = c3 * s1;
        let s3s2c1 = s3 * s2 * c1;
        let c2s1 = c2 * s1;
        let c2c1 = c2 * c1;

        /* Apply all three Euler angles rotations using the three matrices:
         *
         * ⎡  c3 s3 0 ⎤ ⎡ c2  0 -s2 ⎤ ⎡ 1  00 00 ⎤
         * ⎢  s3 c3 0 ⎥ ⎢ 00  1  00 ⎥ ⎢ 0  c1 s1 ⎥
         * ⎣  00 00 1 ⎦ ⎣ s2  0  c2 ⎦ ⎣ 0 -s1 c1 ⎦
         */

        let mvp = this._mvp;
        mvp[0] = c3c2;  mvp[4] = s3c1 + c3s2s1;  mvp[8] = s3s1 - c3s2c1; mvp[12] = 0;
        mvp[1] = -s3c2; mvp[5] = c3c1 - s3s2s1;  mvp[9] = c3s1 + s3s2c1; mvp[13] = 0;
        mvp[2] = s2;    mvp[6] = -c2s1;         mvp[10] = c2c1;          mvp[14] = 0;
        mvp[3] = 0;     mvp[7] = 0;             mvp[11] = 0;             mvp[15] = 1;
    }

    _animateRotation(area_, frameClock) {
        let frameTime = frameClock.get_frame_time();

        if (!this._firstFrameTime) {
            this._firstFrameTime = frameTime;
            return GLib.SOURCE_CONTINUE;
        }

        let angle = (frameTime - this._firstFrameTime) / (GLib.USEC_PER_SEC * 180);
        this._adjustments.forEach(adjustment => (adjustment.value = (adjustment.value + angle) % 360));

        return GLib.SOURCE_CONTINUE;
    }

    _glInit() {
        // We need to ensure that the GdkGLContext is set before calling GL API.
        this._area.make_current();

        // If the GtkGLArea is in an error state we don't do anything.
        if (this._area.get_error())
            return;

        let vertexShader = new Shader({
            type: gl.ShaderType.VERTEX,
            file: directory.get_child('glarea2-vertex.glsl'),
        });

        let fragmentShader = new Shader({
            type: gl.ShaderType.FRAGMENT,
            file: directory.get_child('glarea2-fragment.glsl'),
        });

        // Link the vertex and fragment shaders together.
        this._program = new Program({
            shaders: new ShaderList(vertexShader, fragmentShader),
        });

        // The individual shaders can be destroyed.
        vertexShader.delete();
        fragmentShader.delete();

        // We need to create a VAO to store the other buffers.
        this._vertexArray = new VertexArray();
        this._vertexArray.bind();

        // These is the VBO that holds the vertex data.
        let vertexBuffer = new Buffer({
            binding_target: gl.BufferTarget.ARRAY,
            data_type: gl.DataType.FLOAT,
            usage: gl.BufferDataStoreUsage.STATIC_DRAW,
        });
        vertexBuffer.addData(vertexData);

        // Specify to the buffer how the vertex data is formated.
        let positionLocation = this._program.getAttribLocation('position');
        let colorLocation = this._program.getAttribLocation('color');
        vertexBuffer.setAttribute(positionLocation, 3, false, 6, 0);
        vertexBuffer.setAttribute(colorLocation, 3, false, 6, 3);

        // Reset the state; we will re-enable the VAO when needed.
        this._vertexArray.unbind();

        // The VBO is referenced by the VAO.
        vertexBuffer.delete();

        this.title = gl.getString(gl.Connection.RENDERER) || "Unknown";
    }

    _glFini() {
        // We need to ensure that the GdkGLContext is set before calling GL API.
        this._area.make_current();

        // Skip everything if we're in error state.
        if (this._area.get_error())
            return;

        // Destroy all the resources we created.
        this._vertexArray.delete();
        this._program.delete();
    }

    _glDraw() {
        if (this._area.get_error())
            return false;

        // Clear the viewport; the viewport is automatically resized when
        // the GtkGLArea gets a new size allocation.
        gl.clearColor(0.5, 0.5, 0.5, 1);
        gl.clear(gl.BufferClearMask.COLOR);

        this._program.use();

        // Update the "mvp" matrix we use in the shader.
        this._program.setMatrix4('mvp', false, this._mvp);

        // Draw the three vertices as a triangle.
        this._vertexArray.draw(gl.Primitive.TRIANGLES, 0, 3);

        this._program.unuse();

        // Flush the contents of the pipeline.
        gl.flush();

        return true;
    }

    _animateToggled(button) {
        if (button.active)
            this._tickId = this._area.add_tick_callback(this._animateRotation.bind(this));
        else
            this._area.remove_tick_callback(this._tickId);
    }

    _adjustmentChanged(adjustment) {
        this._rotationAngles[this._adjustments.indexOf(adjustment)] = adjustment.value;
        this._computeMvp();
        this._area.queue_draw();
    }
});

let application = new Gtk.Application();

application.connect('startup', () => {
    let simpleAction = new Gio.SimpleAction({ name: 'quit' });
    simpleAction.connect('activate', () => application.quit());
    application.add_action(simpleAction);
});

application.connect('activate', () => {
    let window = new Window({ application });
    window.present();
});

application.run([]);
