/*
 * SPDX-FileCopyrightText: 2015 Mathieu De Coster
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Adapted from glExamples by Mathieu De Coster.
 * https://github.com/m-decoster/glExamples
 */

imports.gi.versions['Gtk'] = '4.0';
const { GLib, GObject, Gtk, Updwn: gl } = imports.gi;
const ByteArray = imports.byteArray;

const VERTEX_SOURCE = ByteArray.toString(GLib.file_get_contents('triangle-vertex.glsl')[1]);
const FRAGMENT_SOURCE = ByteArray.toString(GLib.file_get_contents('triangle-fragment.glsl')[1]);

const VERTICES = [
    // x     y   r  g  b
    -0.5,  0.5,  0, 0, 0,
     0.5,  0.5,  1, 1, 0,
     0.5, -0.5,  1, 0, 0,
];

function createShader(source, shaderType) {
    let shader = gl.createShader(shaderType);
    gl.shaderSource(shader, [source], null);
    gl.compileShader(shader);

    if (!gl.getShaderiv(shader, gl.COMPILE_STATUS)[0]) {
        let infoLog = gl.getShaderInfoLog(shader, -1);
        gl.deleteShader(shader);
        throw new Error(`Compile failure in shader ${shader}:\n${ByteArray.toString(infoLog)}`);
    }

    return shader;
}

function createShaderProgram(...shaders) {
    let program = gl.createProgram();
    shaders.forEach(shader => gl.attachShader(program, shader));
    gl.linkProgram(program);

    if (!gl.getProgramiv(program, gl.LINK_STATUS)[0]) {
        let infoLog = gl.getProgramInfoLog(program, -1);
        gl.deleteProgram(program);
        throw new Error(`Linking failure:\n${ByteArray.toString(infoLog)}`);
    }

    shaders.forEach(shader => gl.detachShader(program, shader));

    return program;
}

const Area = GObject.registerClass({
}, class _Area extends Gtk.GLArea {
    _initShaders(vertexSource, fragmentSource) {
        let vertex = createShader(vertexSource, gl.VERTEX_SHADER);
        let fragment = createShader(fragmentSource, gl.FRAGMENT_SHADER);
        let program = createShaderProgram(vertex, fragment);

        gl.deleteShader(vertex);
        gl.deleteShader(fragment);
        gl.useProgram(program);
    }

    _initBuffers() {
        [this._vao] = gl.genVertexArrays(1);
        gl.bindVertexArray(this._vao);

        [this._vbo] = gl.genBuffers(1);
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
        gl.bufferDatafv(gl.ARRAY_BUFFER, VERTICES, gl.STATIC_DRAW);

        let size = gl.get_data_type_size(gl.FLOAT);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 5 * size, 0);
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 5 * size, 2 * size);

        gl.bindBuffer(gl.ARRAY_BUFFER, 0);
    }

    vfunc_realize() {
        super.vfunc_realize();

        this.make_current();
        if (this.get_error())
            return;

        gl.clearColor(0.5, 0.5, 0.5, 1.0);

        this._initShaders(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this._initBuffers();
    }

    vfunc_unrealize() {
        this.make_current();
        if (!this.get_error()) {
            gl.deleteBuffers([this._vbo]);
            gl.deleteVertexArrays([this._vao]);
        }

        super.vfunc_unrealize();
    }

    vfunc_render(context) {
        if (this.get_error())
            return false;

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.flush();

        return true;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application,
        title: "Hello Triangle",
        defaultWidth: 640, defaultHeight: 480,
        child: new Area({ hexpand: true, vexpand: true }),
    }).present();
});

application.run([]);
