/*
 * SPDX-FileCopyrightText: 2015 Mathieu De Coster
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Adapted from glExamples by Mathieu De Coster.
 * https://github.com/m-decoster/glExamples
 */

imports.gi.versions['Gtk'] = '4.0';
const { GdkPixbuf, GLib, GObject, Gtk, Updwn: gl } = imports.gi;
const ByteArray = imports.byteArray;

const VERTEX_SOURCE = ByteArray.toString(GLib.file_get_contents('sprite-vertex.glsl')[1]);
const FRAGMENT_SOURCE = ByteArray.toString(GLib.file_get_contents('sprite-fragment.glsl')[1]);

const VERTICES = [
    // x     y   r  g  b   u  v
    -0.5,  0.5,  0, 0, 0,  0, 0,
     0.5,  0.5,  1, 1, 0,  1, 0,
     0.5, -0.5,  1, 0, 0,  1, 1,
    -0.5, -0.5,  1, 1, 0,  0, 1,
];

const INDICES = [
    0, 1, 2,
    2, 3, 0,
];

function createShader(source, shaderType) {
    let shader = gl.createShader(shaderType);
    gl.shaderSource(shader, [source], null);
    gl.compileShader(shader);

    if (!gl.getShaderiv(shader, gl.COMPILE_STATUS)[0]) {
        let infoLog = gl.getShaderInfoLog(shader, -1);
        gl.deleteShader(shader);
        throw new Error(`Compile failure in shader ${shader}:\n${ByteArray.toString(infoLog)}`);
    }

    return shader;
}

function createShaderProgram(...shaders) {
    let program = gl.createProgram();
    shaders.forEach(shader => gl.attachShader(program, shader));
    gl.linkProgram(program);

    if (!gl.getProgramiv(program, gl.LINK_STATUS)[0]) {
        let infoLog = gl.getProgramInfoLog(program, -1);
        gl.deleteProgram(program);
        throw new Error(`Linking failure:\n${ByteArray.toString(infoLog)}`);
    }

    shaders.forEach(shader => gl.detachShader(program, shader));

    return program;
}

function loadImage(filename, index) {
    let [texture] = gl.genTextures(1);
    gl.activeTexture(gl.TEXTURE0 + index);
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

    let pixbuf = GdkPixbuf.Pixbuf.new_from_file(filename);
    // gl.texImage2D takes a GBytes object.
    gl.texImage2D(gl.TEXTURE_2D, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, pixbuf.width, pixbuf.height, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, gl.UNSIGNED_BYTE, pixbuf.read_pixel_bytes());
    // Alternative: gl.texImage2Dubv takes an array of unsigned bytes.
    //gl.texImage2Dubv(gl.TEXTURE_2D, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, pixbuf.width, pixbuf.height, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, gl.UNSIGNED_BYTE, pixbuf.get_pixels());

    gl.bindTexture(gl.TEXTURE_2D, 0);

    return texture;
}

const Area = GObject.registerClass({
}, class _Area extends Gtk.GLArea {
    _initShaders(vertexSource, fragmentSource) {
        let vertex = createShader(vertexSource, gl.VERTEX_SHADER);
        let fragment = createShader(fragmentSource, gl.FRAGMENT_SHADER);
        let program = createShaderProgram(vertex, fragment);

        this._texLocation = gl.getUniformLocation(program, 'tex');

        gl.deleteShader(vertex);
        gl.deleteShader(fragment);
        gl.useProgram(program);
    }

    _initBuffers() {
        [this._vao] = gl.genVertexArrays(1);
        gl.bindVertexArray(this._vao);

        [this._vbo, this._ebo] = gl.genBuffers(2);
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
        gl.bufferDatafv(gl.ARRAY_BUFFER, VERTICES, gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo);
        let iv = new Int32Array(INDICES);
        let ubv = new Uint8Array(iv.buffer);
        gl.bufferDataubv(gl.ELEMENT_ARRAY_BUFFER, ubv, gl.STATIC_DRAW);

        let size = gl.get_data_type_size(gl.FLOAT);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 7 * size, 0);
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 7 * size, 2 * size);
        gl.enableVertexAttribArray(2);
        gl.vertexAttribPointer(2, 2, gl.FLOAT, false, 7 * size, 5 * size);

        gl.bindBuffer(gl.ARRAY_BUFFER, 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0);
    }

    vfunc_realize() {
        super.vfunc_realize();

        this.make_current();
        if (this.get_error())
            return;

        gl.clearColor(0.75, 0.75, 0.75, 1.0);

        this._initShaders(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this._initBuffers();
        this._texture = loadImage('../image.png', 0);
    }

    vfunc_unrealize() {
        this.make_current();
        if (!this.get_error()) {
            gl.deleteBuffers([this._vbo, this._ebo]);
            gl.deleteVertexArrays([this._vao]);
            gl.deleteTextures([this._texture]);
        }

        super.vfunc_unrealize();
    }

    vfunc_render(context) {
        if (this.get_error())
            return false;

        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.bindTexture(gl.TEXTURE_2D, this._texture);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this._ebo);

        gl.uniform1i(this._texLocation, 0);
        gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, 0);

        gl.bindTexture(gl.TEXTURE_2D, 0);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0);
        gl.flush();

        return true;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application,
        title: "Hello Sprite",
        defaultWidth: 640, defaultHeight: 480,
        child: new Area({ hexpand: true, vexpand: true }),
    }).present();
});

application.run([]);
