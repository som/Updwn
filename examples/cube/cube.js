/*
 * SPDX-FileCopyrightText: 2015 Mathieu De Coster
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Adapted from glExamples by Mathieu De Coster.
 * https://github.com/m-decoster/glExamples
 */

imports.gi.versions['Gtk'] = '4.0';
const { GdkPixbuf, GLib, GObject, Graphene, Gtk, Updwn: gl } = imports.gi;
const ByteArray = imports.byteArray;

Graphene.Matrix.prototype.toArray = function() {
    let matrixArray = [];

    for (let j = 0; 4 > j; j++) {
        for (let i = 0; 4 > i; i++)
            matrixArray.push(this.get_value(j, i));
    }

    return matrixArray;
};

const VERTEX_SOURCE = ByteArray.toString(GLib.file_get_contents('cube-vertex.glsl')[1]);
const FRAGMENT_SOURCE = ByteArray.toString(GLib.file_get_contents('cube-fragment.glsl')[1]);

const VERTICES = [
    // x     y     z    r    g    b    u    v
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
     0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
     0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
     0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
     0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,

    -0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
    -0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
     0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
     0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
     0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
     0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,

    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
     0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
     0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
     0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5, -0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5, -0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,

    -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
     0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
     0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 1.0, 0.0,
    -0.5,  0.5,  0.5, 1.0, 1.0, 1.0, 0.0, 0.0,
    -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 0.0, 1.0,
];

function createShader(source, shaderType) {
    let shader = gl.createShader(shaderType);
    gl.shaderSource(shader, [source], null);
    gl.compileShader(shader);

    if (!gl.getShaderiv(shader, gl.COMPILE_STATUS)[0]) {
        let infoLog = gl.getShaderInfoLog(shader, -1);
        gl.deleteShader(shader);
        throw new Error(`Compile failure in shader ${shader}:\n${ByteArray.toString(infoLog)}`);
    }

    return shader;
}

function createShaderProgram(...shaders) {
    let program = gl.createProgram();
    shaders.forEach(shader => gl.attachShader(program, shader));
    gl.linkProgram(program);

    if (!gl.getProgramiv(program, gl.LINK_STATUS)[0]) {
        let infoLog = gl.getProgramInfoLog(program, -1);
        gl.deleteProgram(program);
        throw new Error(`Linking failure:\n${ByteArray.toString(infoLog)}`);
    }

    shaders.forEach(shader => gl.detachShader(program, shader));

    return program;
}

function loadImage(filename, index) {
    let [texture] = gl.genTextures(1);
    gl.activeTexture(gl.TEXTURE0 + index);
    gl.bindTexture(gl.TEXTURE_2D, texture);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

    let pixbuf = GdkPixbuf.Pixbuf.new_from_file(filename);
    // gl.texImage2D takes a GBytes object.
    gl.texImage2D(gl.TEXTURE_2D, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, pixbuf.width, pixbuf.height, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, gl.UNSIGNED_BYTE, pixbuf.read_pixel_bytes());
    // Alternative: gl.texImage2Dubv takes an array of unsigned bytes.
    //gl.texImage2Dubv(gl.TEXTURE_2D, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, pixbuf.width, pixbuf.height, 0, pixbuf.hasAlpha ? gl.RGBA : gl.RGB, gl.UNSIGNED_BYTE, pixbuf.get_pixels());

    gl.bindTexture(gl.TEXTURE_2D, 0);

    return texture;
}

const Area = GObject.registerClass({
}, class _Area extends Gtk.GLArea {
    _init(params) {
        super._init(params);

        this._proj = new Graphene.Matrix().init_perspective(45, 640 / 480, 0.1, 1000);

        let rotationAxis = new Graphene.Vec3().init(0, 1, 1);
        this._model = new Graphene.Matrix().init_rotate(-35, rotationAxis);

        let translationPoint = new Graphene.Point3D().init(0, 0, -4);
        this._view = new Graphene.Matrix().init_translate(translationPoint);

        this.add_tick_callback(this._animate.bind(this));
    }

    _animate(area_, frameClock) {
        let frameTime = frameClock.get_frame_time();

        if (!this._previousFrameTime) {
            this._previousFrameTime = frameTime;
            return GLib.SOURCE_CONTINUE;
        }

        let angle = (frameTime - this._previousFrameTime) / GLib.USEC_PER_SEC;
        this._previousFrameTime = frameTime;

        this._model.rotate(40 * angle, Graphene.vec3_x_axis());
        this.queue_draw();

        return GLib.SOURCE_CONTINUE;
    }

    _initShaders(vertexSource, fragmentSource) {
        let vertex = createShader(vertexSource, gl.VERTEX_SHADER);
        let fragment = createShader(fragmentSource, gl.FRAGMENT_SHADER);
        let program = createShaderProgram(vertex, fragment);

        this._texLocation = gl.getUniformLocation(program, 'tex');
        this._modelLocation = gl.getUniformLocation(program, 'model');
        this._viewLocation = gl.getUniformLocation(program, 'view');
        this._projLocation = gl.getUniformLocation(program, 'projection');

        gl.deleteShader(vertex);
        gl.deleteShader(fragment);
        gl.useProgram(program);
    }

    _initBuffers() {
        [this._vao] = gl.genVertexArrays(1);
        gl.bindVertexArray(this._vao);

        [this._vbo] = gl.genBuffers(1);
        gl.bindBuffer(gl.ARRAY_BUFFER, this._vbo);
        gl.bufferDatafv(gl.ARRAY_BUFFER, VERTICES, gl.STATIC_DRAW);

        let size = gl.get_data_type_size(gl.FLOAT);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 8 * size, 0);
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 8 * size, 3 * size);
        gl.enableVertexAttribArray(2);
        gl.vertexAttribPointer(2, 2, gl.FLOAT, false, 8 * size, 6 * size);

        gl.bindBuffer(gl.ARRAY_BUFFER, 0);
    }

    vfunc_realize() {
        super.vfunc_realize();

        this.make_current();
        if (this.get_error())
            return;

        this.set_has_depth_buffer(true);
        gl.enable(gl.DEPTH_TEST);
        gl.clearColor(0.75, 0.75, 0.75, 1.0);

        this._initShaders(VERTEX_SOURCE, FRAGMENT_SOURCE);
        this._initBuffers();
        this._texture = loadImage('../image.png', 0);
    }

    vfunc_unrealize() {
        this.make_current();
        if (!this.get_error()) {
            gl.deleteBuffers([this._vbo]);
            gl.deleteVertexArrays([this._vao]);
            gl.deleteTextures([this._texture]);
        }

        super.vfunc_unrealize();
    }

    vfunc_render(context) {
        if (this.get_error())
            return false;

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.bindTexture(gl.TEXTURE_2D, this._texture);

        gl.uniformMatrix4fv(this._modelLocation, false, this._model.toArray());
        gl.uniformMatrix4fv(this._viewLocation, false, this._view.toArray());
        gl.uniformMatrix4fv(this._projLocation, false, this._proj.toArray());
        gl.uniform1i(this._texLocation, 0);
        gl.drawArrays(gl.TRIANGLES, 0, 36);

        gl.bindTexture(gl.TEXTURE_2D, 0);
        gl.flush();

        return true;
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application,
        title: "Hello Cube",
        defaultWidth: 640, defaultHeight: 480,
        child: new Area({ hexpand: true, vexpand: true }),
    }).present();
});

application.run([]);
