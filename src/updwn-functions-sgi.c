/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Documentation:
 *
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 *
 * The documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 *
 * The original references are available on <https://github.com/KhronosGroup/OpenGL-Refpages>.
 * Modifications have been applied to fit the API to high-level languages.
 */

#include "updwn-functions-sgi.h"
#include "updwn-functions-utils.h"

/* use malloc */
#include <stdlib.h>

/* use printf (debug) */
//#include <stdio.h>

/**
 * SECTION: updwn-functions-sgi
 * @section_id: updwn-functions-sgi
 * @title: Functions - SGI
 * @short_description: Well-known OpenGL functions whose documentation is licensed under the SGI
 * @see_also: <link linkend="updwn-functions-opl">Functions - OPL</link>
 *
 * &SGISiliconGraphicsKhronosGroup;
 *
 * Full OpenGL documentation on the [Khronos website](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/).
 */

/**
 * updwn_activeTexture:
 * @texture: which texture unit to make active
 *
 * Select active texture unit.
 */
void
updwn_activeTexture (UpdwnTextureUnit texture)
{
  glActiveTexture (texture);
}

/**
 * updwn_bindTexture:
 * @target: the target to which the texture is bound
 * @texture: the name of a texture
 *
 * Bind a named texture to a texturing target.
 */
void
updwn_bindTexture (UpdwnTextureTarget target, uint texture)
{
  glBindTexture (target, texture);
}

/**
 * updwn_blendColor:
 * @red: red component of `GL_BLEND_COLOR`
 * @green: green component of `GL_BLEND_COLOR`
 * @blue: blue component of `GL_BLEND_COLOR`
 * @alpha: alpha component of `GL_BLEND_COLOR`
 *
 * Set the blend color.
 */
void
updwn_blendColor (float red, float green, float blue, float alpha)
{
  glBlendColor (red, green, blue, alpha);
}

/**
 * updwn_blendEquation:
 * @mode: how source and destination colors are combined
 *
 * Specify both the RGB blend equation and the Alpha blend equation for all draw buffers.
 */
void
updwn_blendEquation (UpdwnBlendEquation mode)
{
  glBlendEquation (mode);
}

/**
 * updwn_blendEquationi:
 * @buf: the index of the draw buffer for which to set the blend equation
 * @mode: how source and destination colors are combined
 *
 * Specify both the RGB blend equation and the Alpha blend equation for a single draw buffer.
 */
void
updwn_blendEquationi (uint buf, UpdwnBlendEquation mode)
{
  glBlendEquationi (buf, mode);
}

/**
 * updwn_blendFunc:
 * @sfactor: how the red, green, blue, and alpha source blending factors are computed
 * @dfactor: how the red, green, blue, and alpha destination blending factors are computed
 *
 * Specify pixel arithmetic for all draw buffers.
 */
void
updwn_blendFunc (UpdwnBlendFactor sfactor, UpdwnBlendFactor dfactor)
{
  glBlendFunc (sfactor, dfactor);
}

/**
 * updwn_blendFunci:
 * @buf: the index of the draw buffer for which to set the blend function
 * @sfactor: how the red, green, blue, and alpha source blending factors are computed
 * @dfactor: how the red, green, blue, and alpha destination blending factors are computed
 *
 * Specify pixel arithmetic for a single draw buffer.
 */
void
updwn_blendFunci (uint buf, UpdwnBlendFactor sfactor, UpdwnBlendFactor dfactor)
{
  glBlendFunci (buf, sfactor, dfactor);
}

/**
 * updwn_blendFuncSeparate:
 * @srcRGB: how the red, green, and blue blending factors are computed
 * @dstRGB: how the red, green, and blue destination blending factors are computed
 * @srcAlpha: how the alpha source blending factor is computed
 * @dstAlpha: how the alpha destination blending factor is computed
 *
 * Specify RGB and alpha components pixel arithmetic separately, for all draw buffers.
 */
void
updwn_blendFuncSeparate (UpdwnBlendFactor srcRGB, UpdwnBlendFactor dstRGB, UpdwnBlendFactor srcAlpha, UpdwnBlendFactor dstAlpha)
{
  glBlendFuncSeparate (srcRGB, dstRGB, srcAlpha, dstAlpha);
}

/**
 * updwn_blendFuncSeparatei:
 * @buf: the index of the draw buffer for which to set the blend functions
 * @srcRGB: how the red, green, and blue blending factors are computed
 * @dstRGB: how the red, green, and blue destination blending factors are computed
 * @srcAlpha: how the alpha source blending factor is computed
 * @dstAlpha: how the alpha destination blending factor is computed
 *
 * Specify RGB and alpha components pixel arithmetic separately, for a single draw buffer.
 */
void
updwn_blendFuncSeparatei (uint buf, UpdwnBlendFactor srcRGB, UpdwnBlendFactor dstRGB, UpdwnBlendFactor srcAlpha, UpdwnBlendFactor dstAlpha)
{
  glBlendFuncSeparatei (buf, srcRGB, dstRGB, srcAlpha, dstAlpha);
}

/**
 * updwn_clear:
 * @mask: Bitwise OR of #UpdwnBufferClearMask masks that indicate the buffers to be cleared.
 *
 * Clear buffers to preset values.
 */
void
updwn_clear (uint /* UpdwnBufferClearMask */ mask)
{
  glClear (mask);
}

/**
 * updwn_clearColor:
 * @red: red value used when the color buffers are cleared
 * @green: green value used when the color buffers are cleared
 * @blue: blue value used when the color buffers are cleared
 * @alpha: alpha value used when the color buffers are cleared
 *
 * Specify clear values for the color buffers.
 */
void
updwn_clearColor (float red, float green, float blue, float alpha)
{
  glClearColor (red, green, blue, alpha);
}

/**
 * updwn_clearDepth:
 * @depth: the depth value used when the depth buffer is cleared
 *
 * Specify the clear value for the depth buffer.
 */
void
updwn_clearDepth (double depth)
{
  glClearDepth (depth);
}

/**
 * updwn_clearDepthf:
 * @depth: the depth value used when the depth buffer is cleared
 *
 * Specify the clear value for the depth buffer.
 */
void
updwn_clearDepthf (float depth)
{
  glClearDepth (depth);
}

/**
 * updwn_clearStencil:
 * @s: the index used when the stencil buffer is cleared
 *
 * Specify the clear value for the stencil buffer.
 */
void
updwn_clearStencil (int s)
{
  glClearStencil (s);
}

/**
 * updwn_colorMask:
 * @red: whether the red component is to be written into the frame buffer
 * @green: whether the green component is to be written into the frame buffer
 * @blue: whether the blue component is to be written into the frame buffer
 * @alpha: whether the alpha component is to be written into the frame buffer
 *
 * Enable and disable writing of frame buffer color components for all draw buffers.
 */
void
updwn_colorMask (bool red, bool green, bool blue, bool alpha)
{
  glColorMask (red, green, blue, alpha);
}

/**
 * updwn_colorMaski:
 * @buf: the index of the draw buffer whose color mask to set
 * @red: whether the red component is to be written into the frame buffer
 * @green: whether the green component is to be written into the frame buffer
 * @blue: whether the blue component is to be written into the frame buffer
 * @alpha: whether the alpha component is to be written into the frame buffer
 *
 * Enable and disable writing of frame buffer color components for a specific draw buffer.
 */
void
updwn_colorMaski (uint buf, bool red, bool green, bool blue, bool alpha)
{
  glColorMaski (buf, red, green, blue, alpha);
}

/**
 * updwn_compressedTexImage1Dubv:
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @data
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture image in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexImage1Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, int imageSize, const uint8_t *data)
{
  glCompressedTexImage1D (target, level, internalformat, width, border, imageSize, data);
}

/**
 * updwn_compressedTexImage2Dubv:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @data
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture image in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexImage2Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, int imageSize, const uint8_t *data)
{
  glCompressedTexImage2D (target, level, internalformat, width, height, border, imageSize, data);
}

/**
 * updwn_compressedTexImage3Dubv:
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @data
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture image in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexImage3Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, int imageSize, const uint8_t *data)
{
  glCompressedTexImage3D (target, level, internalformat, width, height, depth, border, imageSize, data);
}

/**
 * updwn_compressedTexSubImage1Dubv:
 * @target: the target texture, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexSubImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexSubImage1Dubv (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTexSubImage1D (target, level, xoffset, width, format, imageSize, data);
}

/**
 * updwn_compressedTexSubImage2Dubv:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexSubImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexSubImage2Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTexSubImage2D (target, level, xoffset, yoffset, width, height, format, imageSize, data);
}

/**
 * updwn_compressedTexSubImage3Dubv:
 * @target: the target texture, must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTexSubImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_compressedTexSubImage3Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage1Dubv:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTextureSubImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage1Dubv (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTextureSubImage1D (texture, level, xoffset, width, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage2Dubv:
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTextureSubImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage2Dubv (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage3Dubv:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data contained in @data
 * @imageSize: the number of unsigned bytes of image data contained in @data
 * @data: (nullable) (array length=imageSize): an array containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * <note>Behaves exactly like the original <function>glCompressedTextureSubImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage3Dubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const uint8_t *data)
{
  glCompressedTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
}

/**
 * updwn_copyTexImage1D:
 * @target: the target texture, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal format of the texture
 * @x: the window x coordinate of the left corner of the row of pixels to be copied
 * @y: the window y coordinate of the left corner of the row of pixels to be copied
 * @width: the width of the texture image, the height is `1`
 * @border: must be 0
 *
 * Copy pixels into a one-dimensional texture image.
 */
void
updwn_copyTexImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int x, int y, int width, int border)
{
  glCopyTexImage1D (target, level, internalformat, x, y, width, border);
}

/**
 * updwn_copyTexImage2D:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal format of the texture
 * @x: the window x coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @y: the window y coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @border: must be 0
 *
 * Copy pixels into a two-dimensional texture image.
 */
void
updwn_copyTexImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int x, int y, int width, int height, int border)
{
  glCopyTexImage2D (target, level, internalformat, x, y, width, height, border);
}

/**
 * updwn_copyTexSubImage1D:
 * @target: the target to which the texture object is bound, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: the texel offset within the texture array
 * @x: the window x coordinate of the left corner of the row of pixels to be copied
 * @y: the window y coordinate of the left corner of the row of pixels to be copied
 * @width: the width of the texture subimage
 *
 * Copy a one-dimensional texture subimage.
 */
void
updwn_copyTexSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int x, int y, int width)
{
  glCopyTexSubImage1D (target, level, xoffset, x, y, width);
}

/**
 * updwn_copyTexSubImage2D:
 * @target: the target to which the texture object is bound
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @x: the window x coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @y: the window y coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 *
 * Copy a two-dimensional texture subimage.
 */
void
updwn_copyTexSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int x, int y, int width, int height)
{
  glCopyTexSubImage2D (target, level, xoffset, yoffset, x, y, width, height);
}

/**
 * updwn_copyTexSubImage3D:
 * @target: the target to which the texture object is bound, must be `GL_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @x: the window x coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @y: the window y coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 *
 * Copy a three-dimensional texture subimage.
 */
void
updwn_copyTexSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height)
{
  glCopyTexSubImage3D (target, level, xoffset, yoffset, zoffset, x, y, width, height);
}

/**
 * updwn_copyTextureSubImage1D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: the texel offset within the texture array
 * @x: the window x coordinate of the left corner of the row of pixels to be copied
 * @y: the window y coordinate of the left corner of the row of pixels to be copied
 * @width: the width of the texture subimage
 *
 * Copy a one-dimensional texture subimage.
 *
 * OpenGL: 4.5
 */
void
updwn_copyTextureSubImage1D (uint texture, int level, int xoffset, int x, int y, int width)
{
  glCopyTextureSubImage1D (texture, level, xoffset, x, y, width);
}

/**
 * updwn_copyTextureSubImage2D:
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @x: the window x coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @y: the window y coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 *
 * Copy a two-dimensional texture subimage.
 *
 * OpenGL: 4.5
 */
void
updwn_copyTextureSubImage2D (uint texture, int level, int xoffset, int yoffset, int x, int y, int width, int height)
{
  glCopyTextureSubImage2D (texture, level, xoffset, yoffset, x, y, width, height);
}

/**
 * updwn_copyTextureSubImage3D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @x: the window x coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @y: the window y coordinate of the lower left corner of the rectangular region of pixels to be copied
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 *
 * Copy a three-dimensional texture subimage.
 *
 * OpenGL: 4.5
 */
void
updwn_copyTextureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height)
{
  glCopyTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, x, y, width, height);
}

/**
 * updwn_cullFace:
 * @mode: whether front- or back-facing facets are candidates for culling
 *
 * Specify whether front- or back-facing facets can be culled.
 */
void
updwn_cullFace (UpdwnFace mode)
{
  glCullFace (mode);
}

/**
 * updwn_deleteTextures:
 * @n: the number of textures to be deleted
 * @textures: (array length=n) (element-type uint): an array of textures to be deleted
 *
 * Delete named textures.
 */
void
updwn_deleteTextures (int n, const uint *textures)
{
  glDeleteTextures (n, textures);
}

/**
 * updwn_depthFunc:
 * @func: the depth comparison function
 *
 * Specify the value used for depth buffer comparisons.
 */
void
updwn_depthFunc (UpdwnComparison func)
{
  glDepthFunc (func);
}

/**
 * updwn_depthMask:
 * @flag: whether the depth buffer is enabled for writing
 *
 * Enable or disable writing into the depth buffer.
 */
void
updwn_depthMask (bool flag)
{
  glDepthMask (flag);
}

/**
 * updwn_depthRange:
 * @nearVal: the mapping of the near clipping plane to window coordinates
 * @farVal: the mapping of the far clipping plane to window coordinates
 *
 * Specify mapping of depth values from normalized device coordinates to window coordinates.
 */
void
updwn_depthRange (double nearVal, double farVal)
{
  glDepthRange (nearVal, farVal);
}

/**
 * updwn_depthRangef:
 * @nearVal: the mapping of the near clipping plane to window coordinates
 * @farVal: the mapping of the far clipping plane to window coordinates
 *
 * Specify mapping of depth values from normalized device coordinates to window coordinates.
 */
void
updwn_depthRangef (float nearVal, float farVal)
{
  glDepthRangef (nearVal, farVal);
}

/**
 * updwn_disable:
 * @cap: a symbolic constant indicating a GL capability
 *
 * Disable a server-side GL capability.
 */
void
updwn_disable (UpdwnCap cap)
{
  glDisable (cap);
}

/**
 * updwn_disablei:
 * @cap: a symbolic constant indicating a GL capability
 * @index: the index of the switch to disable
 *
 * Disable a server-side GL capability.
 */
void
updwn_disablei (UpdwnCap cap, uint index)
{
  glDisablei (cap, index);
}

/**
 * updwn_drawArrays:
 * @mode: what kind of primitives to render
 * @first: the starting index in the enabled arrays
 * @count: the number of indices to be rendered
 *
 * Render primitives from array data.
 */
void
updwn_drawArrays (UpdwnPrimitive mode, int first, int count)
{
  glDrawArrays (mode, first, count);
}

/**
 * updwn_drawElements:
 * @mode: what kind of primitives to render
 * @count: the number of elements to be rendered
 * @type: the type of the values in @indices, must be one of `GL_UNSIGNED_BYTE`, `GL_UNSIGNED_SHORT`, or `GL_UNSIGNED_INT`
 * @indices: (type gint): an offset of the first index in the array in the data store of the buffer currently bound to the `GL_ELEMENT_ARRAY_BUFFER` target
 *
 * Render primitives from array data.
 */
void
updwn_drawElements (UpdwnPrimitive mode, int count, UpdwnDataType type, const void * indices)
{
  glDrawElements (mode, count, type, indices);
}

/**
 * updwn_enable:
 * @cap: a symbolic constant indicating a GL capability
 *
 * Enable a server-side GL capability.
 */
void
updwn_enable (UpdwnCap cap)
{
  glEnable (cap);
}

/**
 * updwn_enablei:
 * @cap: a symbolic constant indicating a GL capability
 * @index: the index of the switch to enable
 *
 * Enable a server-side GL capability.
 */
void
updwn_enablei (UpdwnCap cap, uint index)
{
  glEnablei (cap, index);
}

/**
 * updwn_finish:
 *
 * Block until all GL execution is complete.
 */
void
updwn_finish ()
{
  glFinish ();
}

/**
 * updwn_flush:
 *
 * Force execution of GL commands in finite time.
 */
void
updwn_flush ()
{
  glFlush ();
}

/**
 * updwn_frontFace:
 * @mode: the orientation of front-facing polygons, must be `GL_CW` or `GL_CCW`
 *
 * Define front- and back-facing polygons.
 */
void
updwn_frontFace (uint mode)
{
  glFrontFace (mode);
}

/**
 * updwn_genTextures:
 * @n: the number of texture names to be generated
 * @length: (out): location to store the length of @textures
 * @textures: (out) (array length=length) (element-type uint): an array in which the generated texture names are stored, use free() to free the returned array
 *
 * Generate texture names.
 */
void
updwn_genTextures (int n, int *length, uint **textures)
{
  if (*textures == NULL)
    {
      *length = n;
      *textures = malloc (sizeof (uint) * n);
    }

  glGenTextures (n, *textures);
}

/*
 * glGet family
 */

#define GET_TYPEV(updwn_func, data_type, gl_func) \
void \
updwn_func (UpdwnStateParam pname, int *length, data_type **data) \
{ \
  int count = updwn_get_state_param_count (pname); \
  \
  if (length != NULL) \
    *length = count;\
  \
  if (*data == NULL) \
    *data = malloc (count * sizeof (data_type)); \
  \
  gl_func (pname, *data); \
}

#define GET_TYPEI_V(updwn_func, data_type, gl_func) \
void \
updwn_func (UpdwnStateParam target, uint index, int *length, data_type **data) \
{ \
  int count = updwn_get_state_param_count (target); \
  \
  if (length != NULL) \
    *length = count;\
  \
  if (*data == NULL) \
    *data = malloc (count * sizeof (data_type)); \
  \
  gl_func (target, index, *data); \
}

/**
 * updwn_getBooleanv:
 * @pname: the parameter value to be returned
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEV (updwn_getBooleanv, unsigned char, glGetBooleanv)

/**
 * updwn_getDoublev:
 * @pname: the parameter value to be returned
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEV (updwn_getDoublev, double, glGetDoublev)

/**
 * updwn_getFloatv:
 * @pname: the parameter value to be returned
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEV (updwn_getFloatv, float, glGetFloatv)

/**
 * updwn_getIntegerv:
 * @pname: the parameter value to be returned
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEV (updwn_getIntegerv, int, glGetIntegerv)

/**
 * updwn_getInteger64v:
 * @pname: the parameter value to be returned
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEV (updwn_getInteger64v, int64_t, glGetInteger64v)

/**
 * updwn_getBooleani_v:
 * @target: the parameter value to be returned
 * @index: the index of the particular element being queried
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEI_V (updwn_getBooleani_v, unsigned char, glGetBooleani_v)

/**
 * updwn_getDoublei_v:
 * @target: the parameter value to be returned
 * @index: the index of the particular element being queried
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEI_V (updwn_getDoublei_v, double, glGetDoublei_v)

/**
 * updwn_getFloati_v:
 * @target: the parameter value to be returned
 * @index: the index of the particular element being queried
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEI_V (updwn_getFloati_v, float, glGetFloati_v)

/**
 * updwn_getIntegeri_v:
 * @target: the parameter value to be returned
 * @index: the index of the particular element being queried
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEI_V (updwn_getIntegeri_v, int, glGetIntegeri_v)

/**
 * updwn_getInteger64i_v:
 * @target: the parameter value to be returned
 * @index: the index of the particular element being queried
 * @length: (out) (optional): return the length of @data, or %NULL
 * @data: (out) (array length=length): returns the value or values of the specified parameter, use free() to free the returned array
 *
 * Get the value or values of a selected parameter.
 */
GET_TYPEI_V (updwn_getInteger64i_v, int64_t, glGetInteger64i_v)

#undef GET_TYPEV
#undef GET_TYPEI_V

/**
 * updwn_getCompressedTexImageubv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the compressed texture image data, use free() to free the returned array
 *
 * Get a compressed texture image.
 *
 * <note>Behaves exactly like the original <function>glGetCompressedTexImage ()</function> function, but returns an array of unsigned bytes.</note>
 */
void
updwn_getCompressedTexImageubv (UpdwnTextureTarget target, int level, int *length, uint8_t **pixels)
{
  int bufSize;

  glGetTexLevelParameteriv (target, level, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &bufSize);

  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetCompressedTexImage (target, level, *pixels);
}

/**
 * updwn_getnCompressedTexImageubv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size of the buffer @pixels
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the compressed texture image data, use free() to free the returned array
 *
 * Get a compressed texture image.
 *
 * <note>Behaves exactly like the original <function>glGetnCompressedTexImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnCompressedTexImageubv (UpdwnTextureTarget target, int level, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetnCompressedTexImage (target, level, bufSize, *pixels);
}

/**
 * updwn_getCompressedTextureImageubv:
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size of the buffer @pixels
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the compressed texture image data, use free() to free the returned array
 *
 * Get a compressed texture image.
 *
 * <note>Behaves exactly like the original <function>glGetCompressedTextureImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureImageubv (uint texture, int level, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetCompressedTextureImage (texture, level, bufSize, *pixels);
}

/**
 * updwn_getError:
 *
 * Get error information.
 *
 * Returns: the value of the error flag
 */
UpdwnError
updwn_getError ()
{
  return glGetError();
}

/**
 * updwn_getString:
 * @name: a symbolic constant
 *
 * Get a string describing the current GL connection.
 *
 * Returns: (type utf8): a pointer to a static string describing some aspect of the current GL connection, or `0` if there is an error.
 */
const uint8_t*
updwn_getString (UpdwnConnection name)
{
  return glGetString (name);
}

/**
 * updwn_getStringi:
 * @name: a symbolic constant
 * @index: the index of the string to return
 *
 * Get a string describing the current GL connection.
 *
 * Returns: (type utf8): a pointer to a static string indexed by @index
 */
const uint8_t*
updwn_getStringi (UpdwnConnection name, uint index)
{
  return glGetStringi (name, index);
}

/**
 * updwn_getTexImageubv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the texture image data, use free() to free the returned array
 *
 * Get a texture image.
 *
 * <note>Behaves exactly like the original <function>glGetTexImage ()</function> function, but returns an array of unsigned bytes.</note>
 */
/* FIXME: Pixel storage modes are ignored.
 * "Be sure to take the pixel storage parameters into account, especially GL_PACK_ALIGNMENT."
 */
void
updwn_getTexImageubv (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int *length, uint8_t **pixels)
{
  int elements, width, height;
  int channels = 1;

  glGetTexLevelParameteriv (target, level, GL_TEXTURE_WIDTH, &width);
  glGetTexLevelParameteriv (target, level, GL_TEXTURE_HEIGHT, &height);

  if (!updwn_get_data_type_is_packed (type))
    channels = updwn_get_pixel_format_component_count (format);

  elements = width * height * channels;

  if (length != NULL)
    *length = elements;

  if (*pixels == NULL)
    *pixels = malloc (elements * updwn_get_data_type_size (type));

  glGetTexImage (target, level, format, type, *pixels);
}

/**
 * updwn_getnTexImageubv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size of the buffer @pixels
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the texture image data, use free() to free the returned array
 *
 * Get a texture image.
 *
 * <note>Behaves exactly like the original <function>glGetnTexImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnTexImageubv (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetnTexImage (target, level, format, type, bufSize, *pixels);
}

/**
 * updwn_getTexLevelParameterfv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): returns the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Get texture parameter values for a specific level of detail.
 */
void
updwn_getTexLevelParameterfv (UpdwnTextureTarget target, int level, UpdwnTextureLevelParam pname, int *length, float **params)
{
  int count = updwn_get_texture_level_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (float));

  glGetTexLevelParameterfv (target, level, pname, *params);
}

/**
 * updwn_getTexLevelParameteriv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): returns the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Get texture parameter values for a specific level of detail.
 */
void
updwn_getTexLevelParameteriv (UpdwnTextureTarget target, int level, UpdwnTextureLevelParam pname, int *length, int **params)
{
  int count = updwn_get_texture_level_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (int));

  glGetTexLevelParameteriv (target, level, pname, *params);
}

#define GET_TEX_PARAMETERV(updwn_func, param_type, gl_func) \
void \
updwn_func (UpdwnTextureTarget target, UpdwnTextureParam pname, int *length, param_type **params) \
{ \
  int count = updwn_get_texture_param_count (pname); \
  \
  if (length != NULL) \
    *length = count; \
  \
  if (*params == NULL) \
    *params = malloc (count * sizeof (param_type)); \
  \
  gl_func (target, pname, *params); \
}

/**
 * updwn_getTexParameterfv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEX_PARAMETERV (updwn_getTexParameterfv, float, glTexParameterfv)

/**
 * updwn_getTexParameteriv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEX_PARAMETERV (updwn_getTexParameteriv, int, glTexParameteriv)

/**
 * updwn_getTexParameterIiv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEX_PARAMETERV (updwn_getTexParameterIiv, int, glTexParameterIiv)

/**
 * updwn_getTexParameterIuiv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEX_PARAMETERV (updwn_getTexParameterIuiv, uint, glTexParameterIuiv)

#undef GET_TEX_PARAMETERV

/**
 * updwn_getTextureImageubv:
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size of the buffer @pixels
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the texture image data, use free() to free the returned array
 *
 * Get a texture image.
 *
 * <note>Behaves exactly like the original <function>glGetTextureImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureImageubv (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetTextureImage (texture, level, format, type, bufSize, *pixels);
}

/**
 * updwn_getTextureLevelParameterfv:
 * @texture: the texture object name
 * @level: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): returns the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Get texture parameter values for a specific level of detail.
 */
void
updwn_getTextureLevelParameterfv (uint texture, int level, UpdwnTextureLevelParam pname, int *length, float **params)
{
  int count = updwn_get_texture_level_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (float));

  glGetTextureLevelParameterfv (texture, level, pname, *params);
}

/**
 * updwn_getTextureLevelParameteriv:
 * @texture: the texture object name
 * @level: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): returns the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Get texture parameter values for a specific level of detail.
 */
void
updwn_getTextureLevelParameteriv (uint texture, int level, UpdwnTextureLevelParam pname, int *length, int **params)
{
  int count = updwn_get_texture_level_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (int));

  glGetTextureLevelParameteriv (texture, level, pname, *params);
}

#define GET_TEXTURE_PARAMETERV(updwn_func, param_type, gl_func) \
void \
updwn_func (uint texture, UpdwnTextureParam pname, int *length, param_type **params) \
{ \
  int count = updwn_get_texture_param_count (pname); \
  \
  if (length != NULL) \
    *length = count; \
  \
  if (*params == NULL) \
    *params = malloc (count * sizeof (param_type)); \
  \
  gl_func (texture, pname, *params); \
}

/**
 * updwn_getTextureParameterfv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEXTURE_PARAMETERV (updwn_getTextureParameterfv, float, glTextureParameterfv)

/**
 * updwn_getTextureParameteriv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEXTURE_PARAMETERV (updwn_getTextureParameteriv, int, glTextureParameteriv)

/**
 * updwn_getTextureParameterIiv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEXTURE_PARAMETERV (updwn_getTextureParameterIiv, int, glTextureParameterIiv)

/**
 * updwn_getTextureParameterIuiv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the texture parameters, use free() to free the returned array
 *
 * Get texture parameter values.
 */
GET_TEXTURE_PARAMETERV (updwn_getTextureParameterIuiv, uint, glTextureParameterIuiv)

#undef GET_TEXTURE_PARAMETERV

/**
 * updwn_hint:
 * @target: the behavior to be controlled
 * @mode: the desired behavior
 *
 * Specify implementation-specific hints.
 */
void
updwn_hint (UpdwnHintTarget target, UpdwnHintMode mode)
{
  glHint (target, mode);
}

/**
 * updwn_isEnabled:
 * @cap: a symbolic constant indicating a GL capability
 *
 * Test whether a capability is enabled.
 *
 * Returns: %TRUE if @cap is an enabled capability, %FALSE otherwise
 */
bool
updwn_isEnabled (UpdwnCap cap)
{
  return glIsEnabled (cap);
}

/**
 * updwn_isEnabledi:
 * @cap: a symbolic constant indicating a GL capability
 * @index: the index of the capability
 *
 * Test whether a capability is enabled.
 *
 * Returns: %TRUE if @cap is an enabled capability, %FALSE otherwise
 */
bool
updwn_isEnabledi (UpdwnCap cap, uint index)
{
  return glIsEnabledi (cap, index);
}

/**
 * updwn_isTexture:
 * @texture: a value that may be the name of a texture
 *
 * Determine if a name corresponds to a texture.
 *
 * Notes: a name returned by updwn_genTextures(), but not yet associated with a texture by calling updwn_bindTexture(), is not the name of a texture.
 *
 * Returns: %TRUE if @texture is currently the name of a texture, %FALSE otherwise
 */
bool
updwn_isTexture (uint texture)
{
  return glIsTexture (texture);
}

/**
 * updwn_lineWidth:
 * @width: the width of rasterized lines
 *
 * Specify the width of rasterized lines.
 */
void
updwn_lineWidth (float width)
{
  glLineWidth (width);
}

/**
 * updwn_logicOp:
 * @opcode: a logical operation
 *
 * Specify a logical pixel operation for rendering.
 */
void
updwn_logicOp (UpdwnLogicalOperation opcode)
{
  glLogicOp (opcode);
}

/**
 * updwn_pixelStoref:
 * @pname: the symbolic name of the parameter to be set
 * @param: the value that @pname is set to
 *
 * Set pixel storage modes.
 */
void
updwn_pixelStoref (UpdwnPixelStorageParam pname, float param)
{
  glPixelStoref (pname, param);
}

/**
 * updwn_pixelStorei:
 * @pname: the symbolic name of the parameter to be set
 * @param: the value that @pname is set to
 *
 * Set pixel storage modes.
 */
void
updwn_pixelStorei (UpdwnPixelStorageParam pname, int param)
{
  glPixelStorei (pname, param);
}

/**
 * updwn_pointParameterf:
 * @pname: a single-valued point parameter
 * @param: the value that @pname will be set to
 *
 * Specify point parameters.
 */
void
updwn_pointParameterf (UpdwnPointParam pname, float param)
{
  glPointParameterf (pname, param);
}

/**
 * updwn_pointParameterfv:
 * @pname: a single-valued point parameter
 * @params: (array): a pointer to an array where the value or values to be assigned to @pname are stored
 *
 * Specify point parameters.
 */
void
updwn_pointParameterfv (UpdwnPointParam pname, const float *params)
{
  glPointParameterfv (pname, params);
}

/**
 * updwn_pointParameteri:
 * @pname: a single-valued point parameter
 * @param: the value that @pname will be set to
 *
 * Specify point parameters.
 */
void
updwn_pointParameteri (UpdwnPointParam pname, int param)
{
  glPointParameteri (pname, param);
}

/**
 * updwn_pointParameteriv:
 * @pname: a single-valued point parameter
 * @params: (array): a pointer to an array where the value or values to be assigned to @pname are stored
 *
 * Specify point parameters.
 */
void
updwn_pointParameteriv (UpdwnPointParam pname, const int *params)
{
  glPointParameteriv (pname, params);
}

/**
 * updwn_pointSize
 * @size: the diameter of rasterized points
 *
 * Specify the diameter of rasterized points.
 */
void
updwn_pointSize (float size)
{
  glPointSize (size);
}

/**
 * updwn_polygonMode:
 * @face: the polygons that @mode applies to, must be `GL_FRONT_AND_BACK` for front- and back-facing polygons
 * @mode: how polygons will be rasterized, must be `GL_POINT`, `GL_LINE` or `GL_FILL`
 *
 * Select a polygon rasterization mode.
 */
void
updwn_polygonMode (UpdwnFace face, uint mode)
{
  glPolygonMode (face, mode);
}

/**
 * updwn_polygonOffset:
 * @factor: a scale factor that is used to create a variable depth offset for each polygon
 * @units: value multiplied by an implementation-specific value to create a constant depth offset
 *
 * Set the scale and units used to calculate depth values.
 */
void
updwn_polygonOffset (float factor, float units)
{
  glPolygonOffset (factor, units);
}

/**
 * updwn_sampleCoverage:
 * @value: a single floating-point sample coverage value, clamped to the range `[0, 1]`
 * @invert: a single boolean value representing if the coverage masks should be inverted
 *
 * Specify multisample coverage parameters.
 */
void
updwn_sampleCoverage (float value, bool invert)
{
  glSampleCoverage (value, invert);
}

/**
 * updwn_scissor:
 * @x: the x coordinate of the lower left corner of the scissor box
 * @y: the y coordinate of the lower left corner of the scissor box
 * @width: the width of the scissor box
 * @height: the height of the scissor box
 *
 * Define the scissor box.
 *
 * When a GL context is first attached to a window, @width and @height are set to the dimensions of that window.
 */
void
updwn_scissor (int x, int y, int width, int height)
{
  glScissor (x, y, width, height);
}

/**
 * updwn_stencilFunc:
 * @func: the test function
 * @ref: the reference value for the stencil test, clamped to the range `[0, 2n - 1]` where n is the number of bitplanes in the stencil buffer
 * @mask: a mask that is ANDed with both the reference value and the stored stencil value when the test is done
 *
 * Set front and back function and reference value for stencil testing.
 */
void
updwn_stencilFunc (UpdwnComparison func, int ref, uint mask)
{
  glStencilFunc (func, ref, mask);
}

/**
 * updwn_stencilMask:
 * @mask: a bit mask to enable and disable writing of individual bits in the stencil planes
 *
 * Control the front and back writing of individual bits in the stencil planes.
 */
void
updwn_stencilMask (uint mask)
{
  glStencilMask (mask);
}

/**
 * updwn_stencilOp:
 * @sfail: the action to take when the stencil test fails
 * @dpfail: the stencil action when the stencil test passes, but the depth test fails
 * @dppass: the stencil action when both the stencil test and the depth test pass, or when the stencil test passes and either there is no depth buffer or depth testing is not enabled
 *
 * Set front and back stencil test actions.
 */
void
updwn_stencilOp (UpdwnStencilAction sfail, UpdwnStencilAction dpfail, UpdwnStencilAction dppass)
{
  glStencilOp (sfail, dpfail, dppass);
}

/**
 * updwn_texImage1Dubv:
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a one-dimensional texture image.
 *
 * <note>Behaves exactly like the original <function>glTexImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texImage1Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexImage1D (target, level, internalformat, width, border, format, type, data);
}

/**
 * updwn_texImage2Dubv:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image, must be `0` if @target is `GL_TEXTURE_RECTANGLE` or `GL_PROXY_TEXTURE_RECTANGLE`
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image, or the number of layers in a texture array, in the case of the `GL_TEXTURE_1D_ARRAY` and `GL_PROXY_TEXTURE_1D_ARRAY` targets
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a two-dimensional texture image.
 *
 * <note>Behaves exactly like the original <function>glTexImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texImage2Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexImage2D (target, level, internalformat, width, height, border, format, type, data);
}

/**
 * updwn_texImage3Dubv:
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image, or the number of layers in a texture array
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a three-dimensional texture image.
 *
 * <note>Behaves exactly like the original <function>glTexImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texImage3Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexImage3D (target, level, internalformat, width, height, depth, border, format, type, data);
}

#define TEX_PARAMETER(updwn_func, param_type, gl_func) \
void \
updwn_func (UpdwnTextureTarget target, UpdwnTextureParam pname, param_type param) \
{ \
  gl_func (target, pname, param); \
}

#define TEX_PARAMETERV(updwn_func, param_type, gl_func) \
void \
updwn_func (UpdwnTextureTarget target, UpdwnTextureParam pname, const param_type *params) \
{ \
  gl_func (target, pname, params); \
}

/**
 * updwn_texParameterf:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a single-valued texture parameter
 * @param: the value of pname
 *
 * Set texture parameter.
 */
TEX_PARAMETER (updwn_texParameterf, float, glTexParameterf)

/**
 * updwn_texParameteri:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a single-valued texture parameter
 * @param: the value of pname
 *
 * Set texture parameter.
 */
TEX_PARAMETER (updwn_texParameteri, int, glTexParameteri)

/**
 * updwn_texParameterfv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 */
TEX_PARAMETERV (updwn_texParameterfv, float, glTexParameterfv)

/**
 * updwn_texParameteriv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 */
TEX_PARAMETERV (updwn_texParameteriv, int, glTexParameteriv)

/**
 * updwn_texParameterIiv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 */
TEX_PARAMETERV (updwn_texParameterIiv, int, glTexParameterIiv)

/**
 * updwn_texParameterIuiv:
 * @target: the target to which the texture is bound
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 */
TEX_PARAMETERV (updwn_texParameterIuiv, uint, glTexParameterIuiv)

#undef TEX_PARAMETER
#undef TEX_PARAMETERV

/**
 * updwn_texSubImage1Dubv:
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTexSubImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texSubImage1Dubv (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexSubImage1D (target, level, xoffset, width, format, type, data);
}

/**
 * updwn_texSubImage2Dubv:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTexSubImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texSubImage2Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexSubImage2D (target, level, xoffset, yoffset, width, height, format, type, data);
}

/**
 * updwn_texSubImage3Dubv:
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTexSubImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_texSubImage3Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

#define TEXTURE_PARAMETER(updwn_func, param_type, gl_func) \
void \
updwn_func (uint texture, UpdwnTextureParam pname, param_type param) \
{ \
  gl_func (texture, pname, param); \
}

#define TEXTURE_PARAMETERV(updwn_func, param_type, gl_func) \
void \
updwn_func (uint texture, UpdwnTextureParam pname, const param_type *params) \
{ \
  gl_func (texture, pname, params); \
}

/**
 * updwn_textureParameterf:
 * @texture: the texture object name
 * @pname: the symbolic name of a single-valued texture parameter
 * @param: the value of pname
 *
 * Set texture parameter.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETER (updwn_textureParameterf, float, glTextureParameterf)

/**
 * updwn_textureParameteri:
 * @texture: the texture object name
 * @pname: the symbolic name of a single-valued texture parameter
 * @param: the value of pname
 *
 * Set texture parameter.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETER (updwn_textureParameteri, int, glTextureParameteri)

/**
 * updwn_textureParameterfv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETERV (updwn_textureParameterfv, float, glTextureParameterfv)

/**
 * updwn_textureParameteriv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETERV (updwn_textureParameteriv, int, glTextureParameteriv)

/**
 * updwn_textureParameterIiv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETERV (updwn_textureParameterIiv, int, glTextureParameterIiv)

/**
 * updwn_textureParameterIuiv:
 * @texture: the texture object name
 * @pname: the symbolic name of a texture parameter
 * @params: a pointer to an array where the value or values of pname are stored
 *
 * Set texture parameters.
 *
 * OpenGL: 4.5
 */
TEXTURE_PARAMETERV (updwn_textureParameterIuiv, uint, glTextureParameterIuiv)

#undef TEXTURE_PARAMETER
#undef TEXTURE_PARAMETERV

/**
 * updwn_textureSubImage1Dubv:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTextureSubImage1D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage1Dubv (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTextureSubImage1D (texture, level, xoffset, width, format, type, data);
}

/**
 * updwn_textureSubImage2Dubv:
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTextureSubImage2D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage2Dubv (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, type, data);
}

/**
 * updwn_textureSubImage3Dubv:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable) (array): an array containing the image data, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * <note>Behaves exactly like the original <function>glTextureSubImage3D ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage3Dubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

/**
 * updwn_viewport:
 * @x: the x coordinate of the lower left corner of the viewport rectangle, in pixels
 * @y: the y coordinate of the lower left corner of the viewport rectangle, in pixels
 * @width: the width of the viewport
 * @height: the height of the viewport
 *
 * Set the viewport.
 *
 * When a GL context is first attached to a window, @width and @height are set to the dimensions of that window.
 */
void
updwn_viewport (int x, int y, int width, int height)
{
  glViewport (x, y, width, height);
}
