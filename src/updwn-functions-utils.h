/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __UPDWN_FUNCTIONS_UTILS_H__
#define __UPDWN_FUNCTIONS_UTILS_H__

#include "updwn-enums.h"
#include "updwn-private.h"

bool updwn_get_data_type_is_packed (UpdwnDataType type);
size_t updwn_get_data_type_size (UpdwnDataType type);
uint updwn_get_pixel_format_component_count (UpdwnPixelFormat format);
uint updwn_get_program_param_count (UpdwnProgramParam pname);
uint updwn_get_shader_param_count (UpdwnShaderParam pname);
uint updwn_get_state_param_count (UpdwnStateParam pname);
uint updwn_get_texture_level_param_count (UpdwnTextureLevelParam pname);
uint updwn_get_texture_param_count (UpdwnTextureParam pname);
uint updwn_get_vertex_array_param_count (UpdwnVertexArrayParam pname);
uint updwn_get_vertex_attrib_param_count (UpdwnVertexAttribParam pname);

#endif
