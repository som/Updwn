/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "updwn-functions-utils.h"

/**
 * SECTION: updwn-functions-utils
 * @section_id: updwn-functions-utils
 * @title: Functions - Utils
 * @short_description: Additional functions
 *
 * These functions are not part of the OpenGL API.
 */

/**
 * updwn_get_data_type_is_packed:
 * @type: the data type to be queried
 *
 * Test whether a type is a packed data type.
 *
 * <note>It is used internally by updwn_getTexImageubv() and updwn_getTexImageBytes().</note>
 *
 * Returns: %TRUE if @type is a packed data type, %FALSE otherwise
 */
bool
updwn_get_data_type_is_packed (UpdwnDataType type)
{
  switch (type)
    {
    case GL_FLOAT_32_UNSIGNED_INT_24_8_REV:
    case GL_INT_2_10_10_10_REV:
    case GL_UNSIGNED_BYTE_3_3_2:
    case GL_UNSIGNED_BYTE_2_3_3_REV:
    case GL_UNSIGNED_INT_8_8_8_8:
    case GL_UNSIGNED_INT_8_8_8_8_REV:
    case GL_UNSIGNED_INT_10_10_10_2:
    case GL_UNSIGNED_INT_2_10_10_10_REV:
    case GL_UNSIGNED_INT_24_8:
    case GL_UNSIGNED_INT_10F_11F_11F_REV:
    case GL_UNSIGNED_INT_5_9_9_9_REV:
    case GL_UNSIGNED_SHORT_5_6_5:
    case GL_UNSIGNED_SHORT_5_6_5_REV:
    case GL_UNSIGNED_SHORT_4_4_4_4:
    case GL_UNSIGNED_SHORT_4_4_4_4_REV:
    case GL_UNSIGNED_SHORT_5_5_5_1:
    case GL_UNSIGNED_SHORT_1_5_5_5_REV:
      return 1;
    default:
      return 0;
    }
}

/**
 * updwn_get_data_type_size:
 * @type: the data type to be queried
 *
 * Retrieve the size of the data type, which is meant to be used with updwn_vertexAttribPointer().
 *
 * Returns: the size in bytes of the data type.
 */
size_t
updwn_get_data_type_size (UpdwnDataType type)
{
  switch (type)
    {
    case GL_BYTE:
      return sizeof(GLbyte);
    case GL_DOUBLE:
      return sizeof(GLdouble);
    case GL_FIXED:
      return sizeof(GLfixed);
    case GL_FLOAT:
    case GL_FLOAT_32_UNSIGNED_INT_24_8_REV:
      return sizeof(GLfloat);
    case GL_HALF_FLOAT:
      return sizeof(GLhalf);
    case GL_INT:
    case GL_INT_2_10_10_10_REV:
      return sizeof(GLint);
    case GL_SHORT:
      return sizeof(GLshort);
    case GL_UNSIGNED_BYTE:
    case GL_UNSIGNED_BYTE_3_3_2:
    case GL_UNSIGNED_BYTE_2_3_3_REV:
      return sizeof(GLubyte);
    case GL_UNSIGNED_INT:
    case GL_UNSIGNED_INT_8_8_8_8:
    case GL_UNSIGNED_INT_8_8_8_8_REV:
    case GL_UNSIGNED_INT_10_10_10_2:
    case GL_UNSIGNED_INT_2_10_10_10_REV:
    case GL_UNSIGNED_INT_24_8:
    case GL_UNSIGNED_INT_10F_11F_11F_REV:
    case GL_UNSIGNED_INT_5_9_9_9_REV:
      return sizeof(GLuint);
    case GL_UNSIGNED_SHORT:
    case GL_UNSIGNED_SHORT_5_6_5:
    case GL_UNSIGNED_SHORT_5_6_5_REV:
    case GL_UNSIGNED_SHORT_4_4_4_4:
    case GL_UNSIGNED_SHORT_4_4_4_4_REV:
    case GL_UNSIGNED_SHORT_5_5_5_1:
    case GL_UNSIGNED_SHORT_1_5_5_5_REV:
      return sizeof(GLushort);
    default:
      return 0;
    }
}

/**
 * updwn_get_pixel_format_component_count:
 * @format: the pixel format to be queried
 *
 * Retrieve the number of components of a pixel format.
 *
 * <note>It is used internally by updwn_getTexImageubv() and updwn_getTexImageBytes().</note>
 *
 * Returns: the number of components of @format
 */
uint
updwn_get_pixel_format_component_count (UpdwnPixelFormat format)
{
  switch (format)
    {
    case GL_RED:
    case GL_GREEN:
    case GL_BLUE:
    case GL_RED_INTEGER:
    case GL_GREEN_INTEGER:
    case GL_BLUE_INTEGER:
    case GL_STENCIL_INDEX:
    case GL_DEPTH_COMPONENT:
      return 1;
    case GL_RG:
    case GL_RG_INTEGER:
    /* Useless since the corresponding data type should be packed, according to
     * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glReadPixels.xhtml */
    case GL_DEPTH_STENCIL:
      return 2;
    case GL_RGB:
    case GL_BGR:
    case GL_RGB_INTEGER:
    case GL_BGR_INTEGER:
      return 3;
    default:
      return 4;
    }
}

/**
 * updwn_get_program_param_count
 * @pname: the symbolic name of a program parameter
 *
 * Retrieve the number of values associated with a program parameter.
 *
 * <note>It is used internally by updwn_getProgramiv().</note>
 *
 * Returns: the number of values for @pname
 */
/* https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetProgram.xhtml */
uint
updwn_get_program_param_count (UpdwnProgramParam pname)
{
  switch (pname)
    {
    case GL_COMPUTE_WORK_GROUP_SIZE:
      return 3;
    default:
      return 1;
    }
}

/**
 * updwn_get_shader_param_count
 * @pname: the symbolic name of a shader parameter
 *
 * Retrieve the number of values associated with a shader parameter.
 *
 * <note>It is used internally by updwn_getShaderiv().</note>
 *
 * Returns: the number of values for @pname
 */
/* https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetShader.xhtml */
uint
updwn_get_shader_param_count (UpdwnShaderParam pname)
{
  switch (pname)
    {
    default:
      return 1;
    }
}

/**
 * updwn_get_state_param_count
 * @pname: the symbolic name of a state parameter
 *
 * Retrieve the number of values associated with a state parameter.
 *
 * <note>It is used internally by <function><link linkend="updwn-getBooleanv">updwn_get</link></function> family functions.</note>
 *
 * Returns: the number of values for @pname
 */
/* https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGet.xhtml */
uint
updwn_get_state_param_count (UpdwnStateParam pname)
{
  switch (pname)
    {
    case GL_COMPRESSED_TEXTURE_FORMATS:
      return GL_NUM_COMPRESSED_TEXTURE_FORMATS;
    case GL_PROGRAM_BINARY_FORMATS:
      return GL_NUM_PROGRAM_BINARY_FORMATS;
    case GL_BLEND_COLOR:
    case GL_COLOR_CLEAR_VALUE:
    case GL_COLOR_WRITEMASK:
    case GL_SCISSOR_BOX:
    case GL_VIEWPORT:
      return 4;
    case GL_ALIASED_LINE_WIDTH_RANGE:
    case GL_DEPTH_RANGE:
    case GL_MAX_VIEWPORT_DIMS:
    case GL_POINT_SIZE_RANGE:
    case GL_SMOOTH_LINE_WIDTH_RANGE:
    case GL_VIEWPORT_BOUNDS_RANGE:
      return 2;
    default:
      return 1;
    }
}

/**
 * updwn_get_texture_level_param_count
 * @pname: the symbolic name of a texture level parameter
 *
 * Retrieve the number of values associated with a texture level parameter.
 *
 * <note>It is used internally by <function><link linkend="updwn-getTexLevelParameterfv">updwn_getTexLevelParameter</link></function> family functions.</note>
 *
 * Returns: the number of values for @pname
 */
/* https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetTexLevelParameter.xhtml */
uint
updwn_get_texture_level_param_count (UpdwnTextureLevelParam pname)
{
  switch (pname)
    {
    default:
      return 1;
    }
}

/**
 * updwn_get_texture_param_count
 * @pname: the symbolic name of a texture parameter
 *
 * Retrieve the number of values associated with a texture parameter.
 *
 * <note>It is used internally by <function><link linkend="updwn-getTexParameterfv">updwn_getTexParameter</link></function> family functions.</note>
 *
 * Returns: the number of values for @pname
 */
/* https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetTexParameter.xhtml */
uint
updwn_get_texture_param_count (UpdwnTextureParam pname)
{
  switch (pname)
    {
    case GL_TEXTURE_BORDER_COLOR:
    case GL_TEXTURE_SWIZZLE_RGBA:
      return 4;
    default:
      return 1;
    }
}

/**
 * updwn_get_vertex_array_param_count
 * @pname: the symbolic name of a vertex array parameter
 *
 * Retrieve the number of values associated with a texture parameter.
 *
 * <note>It is used internally by updwn_getVertexArrayiv().</note>
 *
 * Returns: the number of values for @pname
 */
/*
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetVertexArrayiv.xhtml
 */
uint
updwn_get_vertex_array_param_count (UpdwnVertexArrayParam pname)
{
  switch (pname)
    {
    default:
      return 1;
    }
}

/**
 * updwn_get_vertex_attrib_param_count
 * @pname: the symbolic name of a vertex attribute parameter
 *
 * Retrieve the number of values associated with a vertex attribute parameter.
 *
 * <note>It is used internally by <function><link linkend="updwn-getVertexAttribdv">updwn_getVertexAttrib</link></function> family functions.</note>
 *
 * Returns: the number of values for @pname
 */
/*
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetVertexArrayIndexed.xhtml
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetVertexArrayiv.xhtml
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetVertexAttrib.xhtml
 * https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetVertexAttribPointerv.xhtml
 */
uint
updwn_get_vertex_attrib_param_count (UpdwnVertexAttribParam pname)
{
  return pname == GL_CURRENT_VERTEX_ATTRIB ? 4 : 1;
  switch (pname)
    {
    case GL_CURRENT_VERTEX_ATTRIB:
      return 4;
    default:
      return 1;
    }
}
