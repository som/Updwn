/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __UPDWN_FUNCTIONS_BYTES_H__
#define __UPDWN_FUNCTIONS_BYTES_H__

#include "updwn-enums.h"
#include "updwn-private.h"
#include <glib.h>

void updwn_bufferData (UpdwnBufferTarget target, signed long size, const void *data, UpdwnBufferDataStoreUsage usage);
void updwn_bufferDataBytes (UpdwnBufferTarget target, GBytes *dataBytes, UpdwnBufferDataStoreUsage usage);
void updwn_clearBufferData (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_clearBufferDataBytes (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_clearNamedBufferData (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_clearNamedBufferDataBytes (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_clearTexImage (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_clearTexImageBytes (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_clearTexSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_clearTexSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_compressedTexImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, int imageSize, const void *data);
void updwn_compressedTexImage1DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, GBytes *dataBytes);
void updwn_compressedTexImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, int imageSize, const void *data);
void updwn_compressedTexImage2DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, GBytes *dataBytes);
void updwn_compressedTexImage3D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, int imageSize, const void *data);
void updwn_compressedTexImage3DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, GBytes *dataBytes);
void updwn_compressedTexSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTexSubImage1DBytes (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_compressedTexSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTexSubImage2DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_compressedTexSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTexSubImage3DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_compressedTextureSubImage1D (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTextureSubImage1DBytes (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_compressedTextureSubImage2D (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTextureSubImage2DBytes (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_compressedTextureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const void *data);
void updwn_compressedTextureSubImage3DBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, GBytes *dataBytes);
void updwn_getCompressedTexImage (UpdwnTextureTarget target, int level, void *pixels);
void updwn_getCompressedTexImageBytes (UpdwnTextureTarget target, int level, GBytes **pixelBytes);
void updwn_getnCompressedTexImage (UpdwnTextureTarget target, int level, int bufSize, void *pixels);
void updwn_getnCompressedTexImageBytes (UpdwnTextureTarget target, int level, int bufSize, GBytes **pixelBytes);
void updwn_getCompressedTextureImage (uint texture, int level, int bufSize, void *pixels);
void updwn_getCompressedTextureImageBytes (uint texture, int level, int bufSize, GBytes **pixelBytes);
void updwn_getCompressedTextureSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, void *pixels);
void updwn_getCompressedTextureSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, GBytes **pixelBytes);
void updwn_getProgramBinary (uint program, int bufSize, int *length, uint *binaryFormat, void *binary);
void updwn_getProgramBinaryBytes (uint program, uint *binaryFormat, GBytes **binaryBytes);
void updwn_getTexImage (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, void *pixels);
void updwn_getTexImageBytes (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, GBytes **pixelBytes);
void updwn_getnTexImage (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels);
void updwn_getnTexImageBytes (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes);
void updwn_getTextureImage (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels);
void updwn_getTextureImageBytes (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes);
void updwn_getTextureSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels);
void updwn_getTextureSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes);
void updwn_namedBufferData (uint buffer, signed long size, const void *data, UpdwnBufferDataStoreUsage usage);
void updwn_namedBufferDataBytes (uint buffer, GBytes *dataBytes, UpdwnBufferDataStoreUsage usage);
void updwn_programBinary (uint program, uint binaryFormat, const void *binary, int length);
void updwn_programBinaryBytes (uint program, uint binaryFormat, GBytes *binaryBytes);
void updwn_shaderBinary (int count, const uint *shaders, uint binaryFormat, const void *binary, int length);
void updwn_shaderBinaryBytes (int count, const uint *shaders, uint binaryFormat, GBytes *binaryBytes);
void updwn_texImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texImage1DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_texImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texImage2DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_texImage3D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texImage3DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_texSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texSubImage1DBytes (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_texSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texSubImage2DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_texSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_texSubImage3DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_textureSubImage1D (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_textureSubImage1DBytes (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_textureSubImage2D (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_textureSubImage2DBytes (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);
void updwn_textureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data);
void updwn_textureSubImage3DBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes);

#endif
