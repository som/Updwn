/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __UPDWN_FUNCTIONS_SGI_H__
#define __UPDWN_FUNCTIONS_SGI_H__

#include "updwn-enums.h"
#include "updwn-private.h"

void updwn_activeTexture (UpdwnTextureUnit texture);
void updwn_bindTexture (UpdwnTextureTarget target, uint texture);
void updwn_blendColor (float red, float green, float blue, float alpha);
void updwn_blendEquation (UpdwnBlendEquation mode);
void updwn_blendEquationi (uint buf, UpdwnBlendEquation mode);
void updwn_blendFunc (UpdwnBlendFactor sfactor, UpdwnBlendFactor dfactor);
void updwn_blendFunci (uint buf, UpdwnBlendFactor sfactor, UpdwnBlendFactor dfactor);
void updwn_blendFuncSeparate (UpdwnBlendFactor srcRGB, UpdwnBlendFactor dstRGB, UpdwnBlendFactor srcAlpha, UpdwnBlendFactor dstAlpha);
void updwn_blendFuncSeparatei (uint buf, UpdwnBlendFactor srcRGB, UpdwnBlendFactor dstRGB, UpdwnBlendFactor srcAlpha, UpdwnBlendFactor dstAlpha);
void updwn_clear (uint /* UpdwnBufferClearMask */ mask);
void updwn_clearColor (float red, float green, float blue, float alpha);
void updwn_clearDepth (double depth);
void updwn_clearDepthf (float depth);
void updwn_clearStencil (int s);
void updwn_colorMask (bool red, bool green, bool blue, bool alpha);
void updwn_colorMaski (uint buf, bool red, bool green, bool blue, bool alpha);
void updwn_compressedTexImage1Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, int imageSize, const uint8_t *data);
void updwn_compressedTexImage2Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, int imageSize, const uint8_t *data);
void updwn_compressedTexImage3Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, int imageSize, const uint8_t *data);
void updwn_compressedTexSubImage1Dubv (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_compressedTexSubImage2Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_compressedTexSubImage3Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_compressedTextureSubImage1Dubv (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_compressedTextureSubImage2Dubv (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_compressedTextureSubImage3Dubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const uint8_t *data);
void updwn_copyTexImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int x, int y, int width, int border);
void updwn_copyTexImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int x, int y, int width, int height, int border);
void updwn_copyTexSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int x, int y, int width);
void updwn_copyTexSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int x, int y, int width, int height);
void updwn_copyTexSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);
void updwn_copyTextureSubImage1D (uint texture, int level, int xoffset, int x, int y, int width);
void updwn_copyTextureSubImage2D (uint texture, int level, int xoffset, int yoffset, int x, int y, int width, int height);
void updwn_copyTextureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height);
void updwn_cullFace (UpdwnFace mode);
void updwn_deleteTextures (int n, const uint *textures);
void updwn_depthFunc (UpdwnComparison func);
void updwn_depthMask (bool flag);
void updwn_depthRange (double nearVal, double farVal);
void updwn_depthRangef (float nearVal, float farVal);
void updwn_disable (UpdwnCap cap);
void updwn_disablei (UpdwnCap cap, uint index);
void updwn_drawArrays (UpdwnPrimitive mode, int first, int count);
void updwn_drawElements (UpdwnPrimitive mode, int count, UpdwnDataType type, const void *indices);
void updwn_enable (UpdwnCap cap);
void updwn_enablei (UpdwnCap cap, uint index);
void updwn_finish ();
void updwn_flush ();
void updwn_frontFace (uint mode);
void updwn_genTextures (int n, int *length, uint **textures);
void updwn_getBooleanv (UpdwnStateParam pname, int *length, unsigned char **data);
void updwn_getDoublev (UpdwnStateParam pname, int *length, double **data);
void updwn_getFloatv (UpdwnStateParam pname, int *length, float **data);
void updwn_getIntegerv (UpdwnStateParam pname, int *length, int **data);
void updwn_getInteger64v (UpdwnStateParam pname, int *length, int64_t **data);
void updwn_getBooleani_v (UpdwnStateParam target, uint index, int *length, unsigned char **data);
void updwn_getDoublei_v (UpdwnStateParam target, uint index, int *length, double **data);
void updwn_getFloati_v (UpdwnStateParam target, uint index, int *length, float **data);
void updwn_getIntegeri_v (UpdwnStateParam target, uint index, int *length, int **data);
void updwn_getInteger64i_v (UpdwnStateParam target, uint index, int *length, int64_t **data);
void updwn_getCompressedTexImageubv (UpdwnTextureTarget target, int level, int *length, uint8_t **pixels);
void updwn_getnCompressedTexImageubv (UpdwnTextureTarget target, int level, int bufSize, int *length, uint8_t **pixels);
void updwn_getCompressedTextureImageubv (uint texture, int level, int bufSize, int *length, uint8_t **pixels);
UpdwnError updwn_getError ();
const uint8_t* updwn_getString (UpdwnConnection name);
const uint8_t* updwn_getStringi (UpdwnConnection name, uint index);
void updwn_getTexImageubv (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int *length, uint8_t **pixels);
void updwn_getnTexImageubv (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels);
void updwn_getTexLevelParameterfv (UpdwnTextureTarget target, int level, UpdwnTextureLevelParam pname, int *length, float **params);
void updwn_getTexLevelParameteriv (UpdwnTextureTarget target, int level, UpdwnTextureLevelParam pname, int *length, int **params);
void updwn_getTexParameterfv (UpdwnTextureTarget target, UpdwnTextureParam pname, int *length, float **params);
void updwn_getTexParameteriv (UpdwnTextureTarget target, UpdwnTextureParam pname, int *length, int **params);
void updwn_getTexParameterIiv (UpdwnTextureTarget target, UpdwnTextureParam pname, int *length, int **params);
void updwn_getTexParameterIuiv (UpdwnTextureTarget target, UpdwnTextureParam pname, int *length, uint **params);
void updwn_getTextureImageubv (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels);
void updwn_getTextureLevelParameterfv (uint texture, int level, UpdwnTextureLevelParam pname, int *length, float **params);
void updwn_getTextureLevelParameteriv (uint texture, int level, UpdwnTextureLevelParam pname, int *length, int **params);
void updwn_getTextureParameterfv (uint texture, UpdwnTextureParam pname, int *length, float **params);
void updwn_getTextureParameteriv (uint texture, UpdwnTextureParam pname, int *length, int **params);
void updwn_getTextureParameterIiv (uint texture, UpdwnTextureParam pname, int *length, int **params);
void updwn_getTextureParameterIuiv (uint texture, UpdwnTextureParam pname, int *length, uint **params);
void updwn_hint (UpdwnHintTarget target, UpdwnHintMode mode);
bool updwn_isEnabled (UpdwnCap cap);
bool updwn_isEnabledi (UpdwnCap cap, uint index);
bool updwn_isTexture (uint texture);
void updwn_lineWidth (float width);
void updwn_logicOp (UpdwnLogicalOperation opcode);
void updwn_pixelStoref (UpdwnPixelStorageParam pname, float param);
void updwn_pixelStorei (UpdwnPixelStorageParam pname, int param);
void updwn_pointParameterf (UpdwnPointParam pname, float param);
void updwn_pointParameterfv (UpdwnPointParam pname, const float *params);
void updwn_pointParameteri (UpdwnPointParam pname, int param);
void updwn_pointParameteriv (UpdwnPointParam pname, const int *params);
void updwn_pointSize (float size);
void updwn_polygonMode (UpdwnFace face, uint mode);
void updwn_polygonOffset (float factor, float units);
void updwn_sampleCoverage (float value, bool invert);
void updwn_scissor (int x, int y, int width, int height);
void updwn_stencilFunc (UpdwnComparison func, int ref, uint mask);
void updwn_stencilMask (uint mask);
void updwn_stencilOp (UpdwnStencilAction sfail, UpdwnStencilAction dpfail, UpdwnStencilAction dppass);
void updwn_texImage1Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_texImage2Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_texImage3Dubv (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_texParameterf (UpdwnTextureTarget target, UpdwnTextureParam pname, float param);
void updwn_texParameteri (UpdwnTextureTarget target, UpdwnTextureParam pname, int param);
void updwn_texParameterfv (UpdwnTextureTarget target, UpdwnTextureParam pname, const float *params);
void updwn_texParameteriv (UpdwnTextureTarget target, UpdwnTextureParam pname, const int *params);
void updwn_texParameterIiv (UpdwnTextureTarget target, UpdwnTextureParam pname, const int *params);
void updwn_texParameterIuiv (UpdwnTextureTarget target, UpdwnTextureParam pname, const uint *params);
void updwn_texSubImage1Dubv (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_texSubImage2Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_texSubImage3Dubv (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_textureParameterf (uint texture, UpdwnTextureParam pname, float param);
void updwn_textureParameteri (uint texture, UpdwnTextureParam pname, int param);
void updwn_textureParameterfv (uint texture, UpdwnTextureParam pname, const float *params);
void updwn_textureParameteriv (uint texture, UpdwnTextureParam pname, const int *params);
void updwn_textureParameterIiv (uint texture, UpdwnTextureParam pname, const int *params);
void updwn_textureParameterIuiv (uint texture, UpdwnTextureParam pname, const uint *params);
void updwn_textureSubImage1Dubv (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_textureSubImage2Dubv (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_textureSubImage3Dubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_viewport (int x, int y, int width, int height);

#endif
