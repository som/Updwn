/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Documentation:
 *
 * The original references are available on <https://github.com/KhronosGroup/OpenGL-Refpages>.
 * Modifications have been applied to fit the API to language bindings.
 */

#include "updwn-functions-bytes.h"
#include "updwn-functions-utils.h"

/* use printf (debug) */
//#include <stdio.h>

/**
 * SECTION: updwn-functions-bytes
 * @section_id: updwn-functions-bytes
 * @title: Functions - Bytes
 * @short_description: Well-known OpenGL functions dealing with byte data
 *
 * Full OpenGL documentation on the [Khronos website](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/).
 *
 * For introspection purpose, each OpenGL function is replaced with a variant that uses a #GBytes instead of an untyped pointer. There are also typed-array variants in <link linkend="updwn-functions-opl">Functions - OPL</link> and <link linkend="updwn-functions-sgi">Functions - SGI</link>.
 */

/*
 * Copyright 2005 Addison-Wesley
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_bufferData:
 * @target: the target to which the buffer object is bound
 * @size: the size in bytes of the buffer object's new data store
 * @data: (nullable): a pointer to data that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * &OPLAddisonWesleyKhronosGroup;
 * <note>It is replaced with updwn_bufferDataBytes().</note>
 */
void
updwn_bufferData (UpdwnBufferTarget target, signed long size, const void *data, UpdwnBufferDataStoreUsage usage)
{
  glBufferData (target, size, data, usage);
}

/**
 * updwn_bufferDataBytes: (rename-to updwn_bufferData)
 * @target: the target to which the buffer object is bound
 * @dataBytes: (nullable): a #GBytes containing the data that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * &OPLAddisonWesleyKhronosGroup;
 * <note>See also updwn_bufferDatafv() and updwn_bufferDataubv().</note>
 */
void
updwn_bufferDataBytes (UpdwnBufferTarget target, GBytes *dataBytes, UpdwnBufferDataStoreUsage usage)
{
  if (dataBytes == NULL)
    glBufferData (target, 0, NULL, usage);
  else
    glBufferData (target, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL), usage);
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_clearBufferData:
 * @target: the target to which the buffer object is bound
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data in memory addressed by @data
 * @type: the type of the data in memory addressed by @data
 * @data: (nullable): the address of a memory location storing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_clearBufferDataBytes().</note>
 */
void
updwn_clearBufferData (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glClearBufferData (target, internalformat, format, type, data);
}

/**
 * updwn_clearBufferDataBytes: (rename-to updwn_clearBufferData)
 * @target: the target to which the buffer object is bound
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data contained in @dataBytes
 * @type: the type of the data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_clearBufferDataubv().</note>
 */
void
updwn_clearBufferDataBytes (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glClearBufferData (target, internalformat, format, type, NULL);
  else
    glClearBufferData (target, internalformat, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_clearNamedBufferData:
 * @buffer: the name of the buffer object
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data in memory addressed by @data
 * @type: the type of the data in memory addressed by @data
 * @data: (nullable): the address of a memory location storing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_clearNamedBufferDataBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_clearNamedBufferData (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glClearNamedBufferData (buffer, internalformat, format, type, data);
}

/**
 * updwn_clearNamedBufferDataBytes: (rename-to updwn_clearNamedBufferData)
 * @buffer: the name of the buffer object
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data contained in @dataBytes
 * @type: the type of the data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_clearNamedBufferDataubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_clearNamedBufferDataBytes (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glClearNamedBufferData (buffer, internalformat, format, type, NULL);
  else
    glClearNamedBufferData (buffer, internalformat, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_clearTexImage:
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @format: the format of the data whose address in memory is given by @data
 * @type: the type of the data whose address in memory is given by @data
 * @data: (nullable): the address in memory of the data to be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fill all a texture image with a constant value.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_clearTexImageBytes().</note>
 */
void
updwn_clearTexImage (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glClearTexImage (texture, level, format, type, data);
}

/**
 * updwn_clearTexImageBytes: (rename-to updwn_clearTexImage)
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @format: the format of the data contained in @dataBytes
 * @type: the type of the data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the data to be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fill all a texture image with a constant value.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_clearTexImageubv().</note>
 */
void
updwn_clearTexImageBytes (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glClearTexImage (texture, level, format, type, NULL);
  else
    glClearTexImage (texture, level, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_clearTexSubImage:
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @xoffset: the coordinate of the left edge of the region to be cleared
 * @yoffset: the coordinate of the lower edge of the region to be cleared
 * @zoffset: the coordinate of the front of the region to be cleared
 * @width: the width of the region to be cleared
 * @height: the height of the region to be cleared
 * @depth: the depth of the region to be cleared
 * @format: the format of the data whose address in memory is given by @data
 * @type: the type of the data whose address in memory is given by @data
 * @data: (nullable): the address in memory of the data to be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fill all or part of a texture image with a constant value.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_clearTexSubImageBytes().</note>
 */
void
updwn_clearTexSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glClearTexSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

/**
 * updwn_clearTexSubImageBytes: (rename-to updwn_clearTexSubImage)
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @xoffset: the coordinate of the left edge of the region to be cleared
 * @yoffset: the coordinate of the lower edge of the region to be cleared
 * @zoffset: the coordinate of the front of the region to be cleared
 * @width: the width of the region to be cleared
 * @height: the height of the region to be cleared
 * @depth: the depth of the region to be cleared
 * @format: the format of the data contained in @dataBytes
 * @type: the type of the data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the data to be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fill all or part of a texture image with a constant value.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_clearTexSubImageubv().</note>
 */
void
updwn_clearTexSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glClearTexSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, NULL);
  else
    glClearTexSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexImage1D:
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data stored at address @data
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a one-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexImage1DBytes().</note>
 */
void
updwn_compressedTexImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, int imageSize, const void *data)
{
  glCompressedTexImage1D (target, level, internalformat, width, border, imageSize, data);
}

/**
 * updwn_compressedTexImage1DBytes: (rename-to updwn_compressedTexImage1D)
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @dataBytes
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexImage1Dubv().</note>
 */
void
updwn_compressedTexImage1DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexImage1D (target, level, internalformat, width, border, 0, NULL);
  else
    glCompressedTexImage1D (target, level, internalformat, width, border, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexImage2D:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data stored at address @data
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a two-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexImage2DBytes().</note>
 */
void
updwn_compressedTexImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, int imageSize, const void *data)
{
  glCompressedTexImage2D (target, level, internalformat, width, height, border, imageSize, data);
}

/**
 * updwn_compressedTexImage2DBytes: (rename-to updwn_compressedTexImage2D)
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @dataBytes
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @border: must be `0`
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexImage2Dubv().</note>
 */
void
updwn_compressedTexImage2DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexImage2D (target, level, internalformat, width, height, border, 0, NULL);
  else
    glCompressedTexImage2D (target, level, internalformat, width, height, border, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexImage3D:
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data stored at address @data
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image
 * @border: must be `0`
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a three-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexImage3DBytes().</note>
 */
void
updwn_compressedTexImage3D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, int imageSize, const void *data)
{
  glCompressedTexImage3D (target, level, internalformat, width, height, depth, border, imageSize, data);
}

/**
 * updwn_compressedTexImage3DBytes: (rename-to updwn_compressedTexImage3D)
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the format of the compressed image data contained in @dataBytes
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image
 * @border: must be `0`
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture image in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexImage3Dubv().</note>
 */
void
updwn_compressedTexImage3DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexImage3D (target, level, internalformat, width, height, depth, border, 0, NULL);
  else
    glCompressedTexImage3D (target, level, internalformat, width, height, depth, border, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexSubImage1D:
 * @target: the target texture, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexSubImage1DBytes().</note>
 */
void
updwn_compressedTexSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTexSubImage1D (target, level, xoffset, width, format, imageSize, data);
}

/**
 * updwn_compressedTexSubImage1DBytes: (rename-to updwn_compressedTexSubImage1D)
 * @target: the target texture, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexSubImage1Dubv().</note>
 */
void
updwn_compressedTexSubImage1DBytes (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexSubImage1D (target, level, xoffset, width, format, 0, NULL);
  else
    glCompressedTexSubImage1D (target, level, xoffset, width, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexSubImage2D:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexSubImage2DBytes().</note>
 */
void
updwn_compressedTexSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTexSubImage2D (target, level, xoffset, yoffset, width, height, format, imageSize, data);
}

/**
 * updwn_compressedTexSubImage2DBytes: (rename-to updwn_compressedTexSubImage2D)
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexSubImage2Dubv().</note>
 */
void
updwn_compressedTexSubImage2DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexSubImage2D (target, level, xoffset, yoffset, width, height, format, 0, NULL);
  else
    glCompressedTexSubImage2D (target, level, xoffset, yoffset, width, height, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTexSubImage3D:
 * @target: the target texture, must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTexSubImage3DBytes().</note>
 */
void
updwn_compressedTexSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
}

/**
 * updwn_compressedTexSubImage3DBytes: (rename-to updwn_compressedTexSubImage3D)
 * @target: the target texture, must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTexSubImage3Dubv().</note>
 */
void
updwn_compressedTexSubImage3DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, 0, NULL);
  else
    glCompressedTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTextureSubImage1D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTextureSubImage1DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage1D (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTextureSubImage1D (texture, level, xoffset, width, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage1DBytes: (rename-to updwn_compressedTextureSubImage1D)
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage, the height is `1`
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a one-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTextureSubImage1Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage1DBytes (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTextureSubImage1D (texture, level, xoffset, width, format, 0, NULL);
  else
    glCompressedTextureSubImage1D (texture, level, xoffset, width, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTextureSubImage2D:
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTextureSubImage2DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage2D (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage2DBytes: (rename-to updwn_compressedTextureSubImage2D)
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a two-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTextureSubImage2Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage2DBytes (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, 0, NULL);
  else
    glCompressedTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_compressedTextureSubImage3D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data stored at address @data
 * @imageSize: the number of unsigned bytes of image data starting at the address specified by @data
 * @data: (nullable): a pointer to the compressed image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_compressedTextureSubImage3DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, int imageSize, const void *data)
{
  glCompressedTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data);
}

/**
 * updwn_compressedTextureSubImage3DBytes: (rename-to updwn_compressedTextureSubImage3D)
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_2D_ARRAY`, `GL_TEXTURE_3D` or `GL_TEXTURE_CUBE_MAP_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the compressed image data contained in @dataBytes
 * @dataBytes: (nullable): a #GBytes containing the compressed image data, or %NULL
 *
 * Specify a three-dimensional texture subimage in a compressed format.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_compressedTextureSubImage3Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_compressedTextureSubImage3DBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glCompressedTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, 0, NULL);
  else
    glCompressedTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getCompressedTexImage:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @pixels: (out): returns the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getCompressedTexImageBytes().</note>
 */
void
updwn_getCompressedTexImage (UpdwnTextureTarget target, int level, void *pixels)
{
  glGetCompressedTexImage (target, level, pixels);
}

/**
 * updwn_getCompressedTexImageBytes: (rename-to updwn_getCompressedTexImage)
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @pixelBytes: (out): returns a #GBytes containing the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getCompressedTexImageubv().</note>
 */
void
updwn_getCompressedTexImageBytes (UpdwnTextureTarget target, int level, GBytes **pixelBytes)
{
  int bufSize;

  g_return_if_fail (*pixelBytes == NULL);

  glGetTexLevelParameteriv (target, level, GL_TEXTURE_COMPRESSED_IMAGE_SIZE, &bufSize);
  void *pixelData = g_malloc (bufSize);

  glGetCompressedTexImage (target, level, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getnCompressedTexImage:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size of the buffer @pixels
 * @pixels: (out): returns the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getnCompressedTexImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnCompressedTexImage (UpdwnTextureTarget target, int level, int bufSize, void *pixels)
{
  glGetnCompressedTexImage (target, level, bufSize, pixels);
}

/**
 * updwn_getnCompressedTexImageBytes: (rename-to updwn_getnCompressedTexImage)
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size in bytes of @pixelBytes
 * @pixelBytes: (out): returns a #GBytes containing the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getnCompressedTexImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnCompressedTexImageBytes (UpdwnTextureTarget target, int level, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetnCompressedTexImage (target, level, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getCompressedTextureImage:
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size of the buffer @pixels
 * @pixels: (out): returns the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getCompressedTextureImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureImage (uint texture, int level, int bufSize, void *pixels)
{
  glGetCompressedTextureImage (texture, level, bufSize, pixels);
}

/**
 * updwn_getCompressedTextureImageBytes: (rename-to updwn_getCompressedTextureImage)
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @bufSize: the size in bytes of @pixelBytes
 * @pixelBytes: (out): returns a #GBytes containing the compressed texture image
 *
 * Get a compressed texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getCompressedTextureImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureImageBytes (uint texture, int level, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetCompressedTextureImage (texture, level, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_getCompressedTextureSubImage:
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @pixels: (out): returns the texture subimage
 *
 * Retrieve a sub-region of a compressed texture image from a compressed texture object.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_getCompressedTextureSubImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, void *pixels)
{
  glGetCompressedTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, bufSize, pixels);
}

/**
 * updwn_getCompressedTextureSubImageBytes: (rename-to updwn_getCompressedTextureSubImage)
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @pixelBytes: (out): returns a #GBytes containing the texture subimage
 *
 * Retrieve a sub-region of a compressed texture image from a compressed texture object.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_getCompressedTextureSubImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetCompressedTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_getProgramBinary:
 * @program: the name of a program object whose binary representation to retrieve
 * @bufSize: the size of the buffer whose address is given by @binary
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @binary
 * @binaryFormat: (out): the address of a variable to receive a token indicating the format of the binary data returned by the GL
 * @binary: (out): the address of an array into which the GL will return @program's binary representation
 *
 * Get a binary representation of a program object's compiled and linked executable source.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_getProgramBinaryBytes().</note>
 */
void
updwn_getProgramBinary (uint program, int bufSize, int *length, uint *binaryFormat, void *binary)
{
  glGetProgramBinary (program, bufSize, length, binaryFormat, binary);
}

/**
 * updwn_getProgramBinaryBytes: (rename-to updwn_getProgramBinary)
 * @program: the name of a program object whose binary representation to retrieve
 * @binaryFormat: (out): the address of a variable to receive a token indicating the format of the binary data contained in @binaryBytes
 * @binaryBytes: (out): returns a #GBytes containing @program's binary representation
 *
 * Get a binary representation of a program object's compiled and linked executable source.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_getProgramBinaryubv().</note>
 */
void
updwn_getProgramBinaryBytes (uint program, uint *binaryFormat, GBytes **binaryBytes)
{
  int binaryLength;
  int bufSize;

  g_return_if_fail (*binaryBytes == NULL);

  glGetProgramiv (program, GL_PROGRAM_BINARY_LENGTH, &bufSize);
  void *binaryData = g_malloc (bufSize);

  glGetProgramBinary (program, bufSize, &binaryLength, binaryFormat, binaryData);
  *binaryBytes = g_bytes_new_take (binaryData, binaryLength);
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getTexImage:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @pixels: (out): returns the texture image, should be a pointer to an array of the type specified by @type
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getTexImageBytes().</note>
 */
void
updwn_getTexImage (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, void *pixels)
{
  glGetTexImage (target, level, format, type, pixels);
}

/**
 * updwn_getTexImageBytes: (rename-to updwn_getTexImage)
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @pixelBytes: (out): returns a #GBytes containing the texture image
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getTexImageubv().</note>
 */
/* FIXME: Pixel storage modes are ignored.
 * "Be sure to take the pixel storage parameters into account, especially GL_PACK_ALIGNMENT."
 */
void
updwn_getTexImageBytes (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, GBytes **pixelBytes)
{
  int bufSize, width, height;
  int channels = 1;

  g_return_if_fail (*pixelBytes == NULL);

  glGetTexLevelParameteriv (target, level, GL_TEXTURE_WIDTH, &width);
  glGetTexLevelParameteriv (target, level, GL_TEXTURE_HEIGHT, &height);

  if (!updwn_get_data_type_is_packed (type))
    channels = updwn_get_pixel_format_component_count (format);

  bufSize = width * height * channels * updwn_get_data_type_size (type);
  void *pixelData = g_malloc (bufSize);

  glGetTexImage (target, level, format, type, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getnTexImage:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size of the buffer @pixels
 * @pixels: (out): returns the texture image, should be a pointer to an array of the type specified by @type
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getnTexImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnTexImage (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels)
{
  glGetnTexImage (target, level, format, type, bufSize, pixels);
}

/**
 * updwn_getnTexImageBytes: (rename-to updwn_getnTexImage)
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size in bytes of @pixelBytes
 * @pixelBytes: (out): returns a #GBytes containing the texture image
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getnTexImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getnTexImageBytes (UpdwnTextureTarget target, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetnTexImage (target, level, format, type, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_getTextureImage:
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size of the buffer @pixels
 * @pixels: (out): returns the texture image, should be a pointer to an array of the type specified by @type
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_getTextureImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureImage (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels)
{
  glGetTextureImage (texture, level, format, type, bufSize, pixels);
}

/**
 * updwn_getTextureImageBytes: (rename-to updwn_getTextureImage)
 * @texture: the texture object name
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @format: a pixel format for the returned data
 * @type: a pixel type for the returned data
 * @bufSize: the size in bytes of @pixelBytes
 * @pixelBytes: (out): returns a #GBytes containing the texture image
 *
 * Get a texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_getTextureImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureImageBytes (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetTextureImage (texture, level, format, type, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_getTextureSubImage:
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @pixels: (out): returns the texture subimage
 *
 * Retrieve a sub-region of a texture image from a texture object.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_getTextureSubImageBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureSubImage (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, void *pixels)
{
  glGetTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, pixels);
}

/**
 * updwn_getTextureSubImageBytes: (rename-to updwn_getTextureSubImage)
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @pixelBytes: (out): returns a #GBytes containing the texture subimage
 *
 * Retrieve a sub-region of a texture image from a texture object.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_getTextureSubImageubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureSubImageBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, GBytes **pixelBytes)
{
  g_return_if_fail (*pixelBytes == NULL);

  void *pixelData = g_malloc (bufSize);

  glGetTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, pixelData);
  *pixelBytes = g_bytes_new_take (pixelData, bufSize);
}

/*
 * Copyright 2005 Addison-Wesley
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_namedBufferData:
 * @buffer: the name of the buffer object
 * @size: the size in bytes of the buffer object's new data store
 * @data: (nullable): a pointer to data that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * &OPLAddisonWesleyKhronosGroup;
 * <note>It is replaced with updwn_namedBufferDataBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_namedBufferData (uint buffer, signed long size, const void *data, UpdwnBufferDataStoreUsage usage)
{
  glNamedBufferData (buffer, size, data, usage);
}

/**
 * updwn_namedBufferDataBytes: (rename-to updwn_namedBufferData)
 * @buffer: the name of the buffer object
 * @dataBytes: (nullable): a #GBytes containing the data that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * &OPLAddisonWesleyKhronosGroup;
 * <note>See also updwn_namedBufferDatafv() and updwn_namedBufferDataubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_namedBufferDataBytes (uint buffer, GBytes *dataBytes, UpdwnBufferDataStoreUsage usage)
{
  if (dataBytes == NULL)
    glNamedBufferData (buffer, 0, NULL, usage);
  else
    glNamedBufferData (buffer, g_bytes_get_size (dataBytes), g_bytes_get_data (dataBytes, NULL), usage);
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_programBinary:
 * @program: the name of a program object into which to load a program binary
 * @binaryFormat: the format of the binary data in @binary
 * @binary: (nullable): the address of an array containing the binary to be loaded into @program, or %NULL if no binary is to be loaded
 * @length: the number of bytes contained in @binary
 *
 * Load a program object with a program binary.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_programBinaryBytes().</note>
 */
void
updwn_programBinary (uint program, uint binaryFormat, const void *binary, int length)
{
  glProgramBinary (program, binaryFormat, binary, length);
}

/**
 * updwn_programBinaryBytes: (rename-to updwn_programBinary)
 * @program: the name of a program object into which to load a program binary
 * @binaryFormat: the format of the binary data contained in @binaryBytes
 * @binaryBytes: (nullable): a #GBytes containing the binary to be loaded into @program, or %NULL if no binary is to be loaded
 *
 * Load a program object with a program binary.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_programBinaryubv().</note>
 */
void
updwn_programBinaryBytes (uint program, uint binaryFormat, GBytes *binaryBytes)
{
  if (binaryBytes == NULL)
    glProgramBinary (program, binaryFormat, NULL, 0);
  else
    glProgramBinary (program, binaryFormat, g_bytes_get_data (binaryBytes, NULL), g_bytes_get_size (binaryBytes));
}

/*
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 */

/**
 * updwn_shaderBinary:
 * @count: the number of shader object handles contained in @shaders
 * @shaders: (array length=count): the address of an array of shader handles into which to load pre-compiled shader binaries
 * @binaryFormat: the format of the shader binaries contained in @binary
 * @binary: (nullable): the address of an array of bytes containing pre-compiled binary shader code, or %NULL
 * @length: the length of the array whose address is given in @binary
 *
 * Load pre-compiled shader binaries.
 *
 * &OPLKhronosGroup;
 * <note>It is replaced with updwn_shaderBinaryBytes().</note>
 */
void
updwn_shaderBinary (int count, const uint *shaders, uint binaryFormat, const void *binary, int length)
{
  glShaderBinary (count, shaders, binaryFormat, binary, length);
}

/**
 * updwn_shaderBinaryBytes: (rename-to updwn_shaderBinary)
 * @count: the number of shader object handles contained in @shaders
 * @shaders: (array length=count): the address of an array of shader handles into which to load pre-compiled shader binaries
 * @binaryFormat: the format of the shader binaries contained in @binaryBytes
 * @binaryBytes: (nullable): a #GBytes containing pre-compiled binary shader code, or %NULL
 *
 * Load pre-compiled shader binaries.
 *
 * &OPLKhronosGroup;
 * <note>See also updwn_shaderBinaryubv().</note>
 */
void
updwn_shaderBinaryBytes (int count, const uint *shaders, uint binaryFormat, GBytes *binaryBytes)
{
  if (binaryBytes == NULL)
    glShaderBinary (count, shaders, binaryFormat, NULL, 0);
  else
    glShaderBinary (count, shaders, binaryFormat, g_bytes_get_data (binaryBytes, NULL), g_bytes_get_size (binaryBytes));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texImage1D:
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texImage1DBytes().</note>
 */
void
updwn_texImage1D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexImage1D (target, level, internalformat, width, border, format, type, data);
}

/**
 * updwn_texImage1DBytes: (rename-to updwn_texImage1D)
 * @target: the target texture, must be `GL_TEXTURE_1D` or `GL_PROXY_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image, the height is `1`
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texImage1Dubv().</note>
 */
void
updwn_texImage1DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexImage1D (target, level, internalformat, width, border, format, type, NULL);
  else
    glTexImage1D (target, level, internalformat, width, border, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texImage2D:
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image, must be `0` if @target is `GL_TEXTURE_RECTANGLE` or `GL_PROXY_TEXTURE_RECTANGLE`
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image, or the number of layers in a texture array, in the case of the `GL_TEXTURE_1D_ARRAY` and `GL_PROXY_TEXTURE_1D_ARRAY` targets
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texImage2DBytes().</note>
 */
void
updwn_texImage2D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexImage2D (target, level, internalformat, width, height, border, format, type, data);
}

/**
 * updwn_texImage2DBytes: (rename-to updwn_texImage2D)
 * @target: the target texture
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image, must be `0` if @target is `GL_TEXTURE_RECTANGLE` or `GL_PROXY_TEXTURE_RECTANGLE`
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image, or the number of layers in a texture array, in the case of the `GL_TEXTURE_1D_ARRAY` and `GL_PROXY_TEXTURE_1D_ARRAY` targets
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texImage2Dubv().</note>
 */
void
updwn_texImage2DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexImage2D (target, level, internalformat, width, height, border, format, type, NULL);
  else
    glTexImage2D (target, level, internalformat, width, height, border, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texImage3D:
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image, or the number of layers in a texture array
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texImage3DBytes().</note>
 */
void
updwn_texImage3D (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexImage3D (target, level, internalformat, width, height, depth, border, format, type, data);
}

/**
 * updwn_texImage3DBytes: (rename-to updwn_texImage3D)
 * @target: the target texture, must be one of `GL_TEXTURE_3D`, `GL_PROXY_TEXTURE_3D`, `GL_TEXTURE_2D_ARRAY` and `GL_PROXY_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @internalformat: the internal pixel format that specifies the number of color components in the texture
 * @width: the width of the texture image
 * @height: the height of the texture image
 * @depth: the depth of the texture image, or the number of layers in a texture array
 * @border: must be `0`
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a three-dimensional texture image.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texImage3Dubv().</note>
 */
void
updwn_texImage3DBytes (UpdwnTextureTarget target, int level, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, int border, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexImage3D (target, level, internalformat, width, height, depth, border, format, type, NULL);
  else
    glTexImage3D (target, level, internalformat, width, height, depth, border, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texSubImage1D:
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texSubImage1DBytes().</note>
 */
void
updwn_texSubImage1D (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexSubImage1D (target, level, xoffset, width, format, type, data);
}

/**
 * updwn_texSubImage1DBytes: (rename-to updwn_texSubImage1D)
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texSubImage1Dubv().</note>
 */
void
updwn_texSubImage1DBytes (UpdwnTextureTarget target, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexSubImage1D (target, level, xoffset, width, format, type, NULL);
  else
    glTexSubImage1D (target, level, xoffset, width, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texSubImage2D:
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texSubImage2DBytes().</note>
 */
void
updwn_texSubImage2D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexSubImage2D (target, level, xoffset, yoffset, width, height, format, type, data);
}

/**
 * updwn_texSubImage2DBytes: (rename-to updwn_texSubImage2D)
 * @target: the target to which the texture is bound
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texSubImage2Dubv().</note>
 */
void
updwn_texSubImage2DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexSubImage2D (target, level, xoffset, yoffset, width, height, format, type, NULL);
  else
    glTexSubImage2D (target, level, xoffset, yoffset, width, height, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_texSubImage3D:
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_texSubImage3DBytes().</note>
 */
void
updwn_texSubImage3D (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

/**
 * updwn_texSubImage3DBytes: (rename-to updwn_texSubImage3D)
 * @target: the target to which the texture is bound, must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_texSubImage3Dubv().</note>
 */
void
updwn_texSubImage3DBytes (UpdwnTextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, NULL);
  else
    glTexSubImage3D (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_textureSubImage1D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_textureSubImage1DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage1D (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTextureSubImage1D (texture, level, xoffset, width, format, type, data);
}

/**
 * updwn_textureSubImage1DBytes: (rename-to updwn_textureSubImage1D)
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_1D`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @width: the width of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a one-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_textureSubImage1Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage1DBytes (uint texture, int level, int xoffset, int width, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTextureSubImage1D (texture, level, xoffset, width, format, type, NULL);
  else
    glTextureSubImage1D (texture, level, xoffset, width, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_textureSubImage2D:
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_textureSubImage2DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage2D (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, type, data);
}

/**
 * updwn_textureSubImage2DBytes: (rename-to updwn_textureSubImage2D)
 * @texture: the texture object name
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a two-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_textureSubImage2Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage2DBytes (uint texture, int level, int xoffset, int yoffset, int width, int height, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, type, NULL);
  else
    glTextureSubImage2D (texture, level, xoffset, yoffset, width, height, format, type, g_bytes_get_data (dataBytes, NULL));
}

/*
 * Copyright 1991-2006 Silicon Graphics, Inc.
 * Copyright 2010-2014 Khronos Group
 * This documentation is licensed under the SGI Free Software B License.
 * For details, see <https://khronos.org/registry/OpenGL-Refpages/LICENSES/LicenseRef-FreeB.txt>.
 */

/**
 * updwn_textureSubImage3D:
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @data: (nullable): a pointer to the image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>It is replaced with updwn_textureSubImage3DBytes().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage3D (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const void *data)
{
  glTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

/**
 * updwn_textureSubImage3DBytes: (rename-to updwn_textureSubImage3D)
 * @texture: the texture object name, whose effective target must be `GL_TEXTURE_3D` or `GL_TEXTURE_2D_ARRAY`
 * @level: the level-of-detail number, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset:  a texel offset in the x direction within the texture array
 * @yoffset:  a texel offset in the y direction within the texture array
 * @zoffset:  a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage
 * @height: the height of the texture subimage
 * @depth: the depth of the texture subimage
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @dataBytes: (nullable): a #GBytes containing the image data in memory, or %NULL
 *
 * Specify a three-dimensional texture subimage.
 *
 * &SGISiliconGraphicsKhronosGroup;
 * <note>See also updwn_textureSubImage3Dubv().</note>
 *
 * OpenGL: 4.5
 */
void
updwn_textureSubImage3DBytes (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, GBytes *dataBytes)
{
  if (dataBytes == NULL)
    glTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, NULL);
  else
    glTextureSubImage3D (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, g_bytes_get_data (dataBytes, NULL));
}
