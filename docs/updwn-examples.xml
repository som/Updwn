<?xml version="1.0"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN" "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
  <!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
  <!ENTITY triangle SYSTEM "triangle.js">
  <!ENTITY triangleFragment SYSTEM "triangle-fragment.glsl">
  <!ENTITY triangleVertex SYSTEM "triangle-vertex.glsl">
  <!ENTITY sprite SYSTEM "sprite.js">
  <!ENTITY spriteFragment SYSTEM "sprite-fragment.glsl">
  <!ENTITY spriteVertex SYSTEM "sprite-vertex.glsl">
  <!ENTITY cube SYSTEM "cube.js">
  <!ENTITY cubeFragment SYSTEM "cube-fragment.glsl">
  <!ENTITY cubeVertex SYSTEM "cube-vertex.glsl">
  <!ENTITY cubemaps SYSTEM "cubemaps.js">
  <!ENTITY cubemapsFragment SYSTEM "cubemaps-fragment.glsl">
  <!ENTITY cubemapsVertex SYSTEM "cubemaps-vertex.glsl">
]>
<chapter id="examples">
  <title>Examples</title>

  <refentry>
    <refmeta>
      <refentrytitle>Triangle</refentrytitle>
    </refmeta>
    <refnamediv>
      <refdescriptor>
        This example shows the minimal code needed to render a simple colored triangle.
        <note>
          It is adapted from <ulink url="https://github.com/m-decoster/glExamples">glExamples</ulink> by <person><personname><firstname>Mathieu</firstname><surname>De Coster</surname></personname></person>.
        </note>
      </refdescriptor>
    </refnamediv>
    <refsect1>
      <mediaobject>
        <imageobject>
          <imagedata fileref="triangle.png" format="PNG" width="50%"/>
        </imageobject>
      </mediaobject>
      <formalpara>
        <title><filename>triangle-vertex.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&triangleVertex;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>triangle-fragment.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&triangleFragment;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>triangle.js</filename></title>
        <informalexample>
          <programlisting language="JavaScript">&triangle;</programlisting>
        </informalexample>
      </formalpara>
    </refsect1>
  </refentry>

  <refentry>
    <refmeta>
      <refentrytitle>Sprite</refentrytitle>
    </refmeta>
    <refnamediv>
      <refdescriptor>
        This example shows the minimal code needed to render a textured quad. It also uses <function>glDrawElements</function> to reduce duplication of vertices.
        <note>
          It is adapted from <ulink url="https://github.com/m-decoster/glExamples">glExamples</ulink> by <person><personname><firstname>Mathieu</firstname><surname>De Coster</surname></personname></person>.
        </note>
      </refdescriptor>
    </refnamediv>
    <refsect1>
      <mediaobject>
        <imageobject>
          <imagedata fileref="sprite.png" format="PNG" width="50%"/>
        </imageobject>
      </mediaobject>
      <formalpara>
        <title><filename>sprite-vertex.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&spriteVertex;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>sprite-fragment.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&spriteFragment;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>sprite.js</filename></title>
        <informalexample>
          <programlisting language="JavaScript">&sprite;</programlisting>
        </informalexample>
      </formalpara>
    </refsect1>
  </refentry>

  <refentry>
    <refmeta>
      <refentrytitle>Cube</refentrytitle>
    </refmeta>
    <refnamediv>
      <refdescriptor>
        This example shows the minimal code needed to render a textured quad. It also uses depth testing to make sure the cube is rendered correctly.
        <note>
          It is adapted from <ulink url="https://github.com/m-decoster/glExamples">glExamples</ulink> by <person><personname><firstname>Mathieu</firstname><surname>De Coster</surname></personname></person>.
        </note>
      </refdescriptor>
    </refnamediv>
    <refsect1>
      <mediaobject>
        <imageobject>
          <imagedata fileref="cube.png" format="PNG" width="50%"/>
        </imageobject>
      </mediaobject>
      <formalpara>
        <title><filename>cube-vertex.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&cubeVertex;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>cube-fragment.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&cubeFragment;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>cube.js</filename></title>
        <informalexample>
          <programlisting language="JavaScript">&cube;</programlisting>
        </informalexample>
      </formalpara>
    </refsect1>
  </refentry>

  <refentry>
    <refmeta>
      <refentrytitle>Cubemaps</refentrytitle>
    </refmeta>
    <refnamediv>
      <refdescriptor>
        This example shows how to create a skybox with cubemaps. It demonstrates loading 6 images into a cubemap, and it demonstrates how to draw the skybox behind everything else.
        <note>
          It is adapted from <ulink url="https://github.com/m-decoster/glExamples">glExamples</ulink> by <person><personname><firstname>Mathieu</firstname><surname>De Coster</surname></personname></person>.
        </note>
      </refdescriptor>
    </refnamediv>
    <refsect1>
      <mediaobject>
        <imageobject>
          <imagedata fileref="cubemaps.png" format="PNG" width="50%"/>
        </imageobject>
      </mediaobject>
      <formalpara>
        <title><filename>cubemaps-vertex.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&cubemapsVertex;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>cubemaps-fragment.glsl</filename></title>
        <informalexample>
          <programlisting language="GLSL">&cubemapsFragment;</programlisting>
        </informalexample>
      </formalpara>
      <formalpara>
        <title><filename>cubemaps.js</filename></title>
        <informalexample>
          <programlisting language="JavaScript">&cubemaps;</programlisting>
        </informalexample>
      </formalpara>
    </refsect1>
  </refentry>

</chapter>
