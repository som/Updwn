# Updwn

An introspectable OpenGL wrapper library.

It is meant to be used with high-level languages. An example of use is to draw into a `GtkGlArea` widget from GJS.

## Install

Run the following command in the source code directory to build and install the library with the documentation:

```sh
meson -Dgtk-doc=true build && cd buid && ninja && sudo ninja install
```

Run the following commands in the source code directory to uninstall the library and the documentation:

```sh
cd build && sudo ninja uninstall
sudo rm -r /usr/local/share/gtk-doc/html/updwn
```

## Licence

A large part of the documentation contained in the GTK-Doc comments is taken from OpenGL and is therefore licenced under either the OPL or the SGI licence. It should be explicitly stated. Everything else is licenced under the GPLv3+.
